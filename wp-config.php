<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'aq');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'b)UV0z0Z(o6W|9[}n>{u6X#wNY&>#m)uZ+/z[rw_M:^}HvWG3Kl)ZR-YUbo|lP3b');
define('SECURE_AUTH_KEY',  '?gz8Ubq;gRH>;Gk2vES&|y~]1XB^:e~T|c6o1*6`>>eKAZX5OoP9+%9wd.SHSv@d');
define('LOGGED_IN_KEY',    ':#msaPHqFuoZE6wl ?~_)=0G=/7`_|kdty#g_^BgMcwiS%x/YLv$-`7+2R@VUM+u');
define('NONCE_KEY',        '*j3P`&/GejYmEmC~_JlO-(-QhB&b#`jz8*@]8P,Xcs%w9UlCx ~Gt,?hZy`3uL`7');
define('AUTH_SALT',        'pSzzg|_%$!<ta/&j/xeb%^-*`)-Si4H{besl%RU*&26TnQtd_LSJ~%6u#)%@@Ch6');
define('SECURE_AUTH_SALT', '$<RA~EsAR.3mSD+:vs9)ut|@cby;K:bP93`Qi?$,Sh]rgkAs>.7-.y^`l}@3L_d8');
define('LOGGED_IN_SALT',   'oaKgz$GT(e0[=O luF:AH3|Oo~.7twul`,[srF`*r1;.yJ%F@h@:kq-98nTy.{Du');
define('NONCE_SALT',       '2/pM9j#0U!][x<P2*U, oH~=wd&,rau:?a=Z(n]WWBd;GD=#fQMX_jKC6~3KI!J:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
