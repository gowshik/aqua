<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
			<div class="container px-5">
				<div class="row justify-content-center">
					<div class="col-lg-8 col-md-10">
						<?php
						/* Start the Loop */
						while ( have_posts() ) : the_post();
							// $blocation = get_field('gha_builder_location');
							// echo $blocation['address'];
							// print_r(get_field('gha_builder_location'));
							get_template_part( 'template-parts/post/content', get_post_format() );

						endwhile; // End of the loop.
						?>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
	<!--?php get_sidebar(); ?-->
</div><!-- .wrap -->

<?php get_footer();
