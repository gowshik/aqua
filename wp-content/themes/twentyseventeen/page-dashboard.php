<?php
/**
 * Template Name: Dashboard
 *
*/
//get_header();
wp_head();
while ( have_posts() ) : the_post();
?>
<link href="https://file.myfontastic.com/5kX8TPLEh2hK7BdGxf37rg/icons.css" rel="stylesheet">

<div class="builder-portal-header mobile-head">
	<div class="container-fluid px-5">
		<div class="row position-relative">
			<div class="col-8 d-flex col-lg-3 col-xl-2 builder-portalh">
				<span class="d-flex d-lg-none my-auto text-white mr-2 ghaico-ghaico-portal-menu"></span>
				<h2 class="my-auto mx-0 text-white">Aqualand</h2>
			</div>

			<div class="col-6 d-none d-lg-flex col-lg-6 col-xl-8">
				<div class="portal-search w-100 my-auto">
					<?php get_search_form(); ?>
				</div>
			</div>

			<div class="col-3 d-none d-lg-flex col-lg-3 col-xl-2 builder-account">
				<div class="my-auto w-100 text-white text-right">
					<!--img src="<?php echo get_template_directory_uri() . '/assets/images/gha-icon-portal.png' ?>"-->
					<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>
				</div>
			</div>

			<div class="gha-builder-pdetails p-4 bg-white position-absolute">
				<p class="text-medium font-weight-bold">My details</p>
				<table class="mb-3">
					<tbody>
						<tr>
							<td class="py-1" style="width: 80px;">Phone</td>
							<td class="font-weight-bold">1300 724 661</td>
						</tr>

						<tr>
							<td class="py-1" style="width: 80px;">Email</td>
							<td class="font-weight-bold">support@greenhomesaustralia.com.au</td>
						</tr>

						<tr>
							<td class="py-1" style="width: 80px;">Address</td>
							<td class="font-weight-bold">125 Byng Street, Orange, NSW 2800</td>
						</tr>
					</tbody>
				</table>

				<p class="text-medium font-weight-bold">
					<a href="<?php echo wp_logout_url(home_url( '/builder-area' )); ?>">Logout <span class="ghaico-ghaico-sign-out"></span></a>
				</p>
			</div>

		</div>
	</div>
</div>

<div class="builder-potal-body">
	<div class="container-fluid">
		<div class="row">





			<div class="d-lg-block col-lg-2 portal-menu pt-4 px-0 bg-white menu-small">

				<div class="d-block  d-lg-none my-3 px-3 w-100">
					<div class="sm-bdetals" style="border-bottom: 1px solid #DDDDDD; padding-bottom: 1rem;">
						<div>
							<img class="mr-2" src="<?php echo get_template_directory_uri() . '/assets/images/gha-portal-icon-alt.png' ?>"> GHA Orange
							<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>
							<span class="icon-chevron-down"></span>

						</div>

						<div class="gha-builder-pdetails p-4">
							<p class="text-medium font-weight-bold">My details</p>
							<table class="mb-3">
								<tbody>
									<tr>
										<td class="py-1" style="width: 80px;">Phone</td>
										<td class="font-weight-bold">1300 724 661</td>
									</tr>

									<tr>
										<td class="py-1" style="width: 80px;">Email</td>
										<td class="font-weight-bold">support@greenhomesaustralia.com.au</td>
									</tr>

									<tr>
										<td class="py-1" style="width: 80px;">Address</td>
										<td class="font-weight-bold">125 Byng Street, Orange, NSW 2800</td>
									</tr>
								</tbody>
							</table>

							<p class="text-medium font-weight-bold">
								<a href="#">Logout <span class="ghaico-ghaico-sign-out"></span></a>
							</p>
						</div>

					</div>
				</div>
				<div class="logo-holder" style="padding-bottom:30px;">
					<center><img src="<?php echo get_template_directory_uri() . '/assets/images/logo-white.png'; ?>"></center>
				</div>

				<?php
					wp_nav_menu( array(
					    'theme_location' => 'builder_area',
					) );
				?>
			</div>

			<div class=" col-lg-10 " style="margin-bottom: 80px;padding-right:5px;padding-left:2px">
				<div class="builder-portal-header desktop-head">
				<div class="row position-relative">


					<div class="col-10 d-none d-lg-flex col-lg-10 ">
						<div class="portal-search w-100 my-auto">
							<?php get_search_form(); ?>
						</div>
					</div>

					<div class="col-2 d-none d-lg-flex col-lg-2  builder-account">
						<div class="my-auto w-100 text-white text-right">

							<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>

						</div>
					</div>

					<div class="gha-builder-pdetails p-4 bg-white position-absolute">
						<p class="text-medium font-weight-bold" style="color:#274279;">My details</p>
						<table class="mb-3">
							<tbody>
								<tr>
									<td class="py-1" style="width: 80px;">Phone</td>
									<td class="font-weight-bold">02 9228 1888</td>
								</tr>

								<tr>
									<td class="py-1" style="width: 80px;">Email</td>
									<td class="font-weight-bold">info@aqualand.com.au</td>
								</tr>

								<tr>
									<td class="py-1" style="width: 80px;">Address</td>
									<td class="font-weight-bold">Tower One, Level 47,<br> 100 Barangaroo Avenue, <br>Barangaroo NSW 2000</td>
								</tr>
							</tbody>
						</table>

						<p class="text-medium font-weight-bold">
							<a style="color:#274279;" href="<?php echo wp_logout_url(home_url( '/aqualand' )); ?>">Logout <span class="ghaico-ghaico-sign-out"></span></a>
						</p>
					</div>
				</div>
				</div>

				<div class="builder-portal-resources">
					<ul>
						<?php
							if( have_rows('gha_builder_area_resources') ) :
								while ( have_rows('gha_builder_area_resources') ) : the_row();
						?>
									<li class="mb-4 bg-white"><a class="d-block p-4 ghaico-ghaico-download" href="<?php the_sub_field('gha_builder_area_resource_link'); ?>" target="_blank"><?php the_sub_field('gha_builder_area_resource_title'); ?></a></li>
						<?php
								endwhile;
							endif;
						?>
					</ul>

          <section id="posts" style="padding-top:40px;padding-bottom:100px;">
						<div class=" container project-menu project-tiles" style="max-width:100%">

							<?php echo do_shortcode('[wonderplugin_slider id=1]'); ?>


						</div>

            <div class=" container project-menu project-tiles" style="max-width:100%">

              <div class="row different-tile">
                <div class="column">
                  <a href="<?php echo esc_url( home_url( '/forms-policies-and-procedures' ) ); ?>">
                    <div class="project-bg" style="background-image: url(../../wp-content/uploads/2018/07/21_McPherson_Thornlands_Drone2.jpg);"></div>
                  <div class="project-info"><h3>POLCIES & PROCDEDURES</h3></div>
                </a>
                </div>
                <div class="column">
                  <a href="<?php echo esc_url( home_url( '/projects' ) ); ?>">
                    <div class="project-bg" style="background-image: url(../../wp-content/uploads/2018/07/21_McPherson_Thornlands_Kitchenhall.jpg);"></div>
                  <div class="project-info"><h3>PROJECTS</h3></div>
                </a>
                </div>
                <div class="column">
                  <a href="<?php echo esc_url( home_url( '/corporate-structure' ) ); ?>">
                    <div class="project-bg" style="background-image: url(../../wp-content/uploads/2018/07/customhome-lifestyle-1.jpg);"></div>
                  <div class="project-info"><h3>CORPORATE STRUCTURE</h3></div>
                </a>
                </div>
                <div class="column">
                  <a href="<?php echo esc_url( home_url( '/media-social' ) ); ?>">
                    <div class="project-bg" style="background-image: url(../../wp-content/uploads/2018/07/6-movein.jpg);"></div>
                  <div class="project-info"><h3>MEDIA & SOCIAL</h3></div>
                </a>
                </div>

              </div>

            </div>
          <div class=" container project-menu project-tiles" style="max-width:100%">
          <div class="row">
            <div class="col-3 announcements">
              <div class="announce-list">
              <h4>COMPANY ANNOUNCEMENTS</h4>
              <?php $args3 = array( 'order' => 'DESC','posts_per_page' => '3','post_type'=>'announcement');?>
              <?php $q =new WP_query ( $args3 );
              if ($q->have_posts() ) : ?>

                  <?php while ($q->have_posts()) : $q->the_post(); ?>


                      <div class="post-wrap">
                        <div>
                      <p class="post_tag">AQ GROUP</p>
                        <p class="post_title"><?php the_title(); ?></p>
                        <p class="post_date"><?php echo get_the_date(); ?></p>
                      </div>
                        <div class="div_separator"></div>
                      </div>


                  <?php endwhile; ?>

                <?php endif; ?>
            </div>
          </div>
            <div class="col-3 events">
                <div class="event-list">
              <h4>UPCOMING EVENTS</h4>
              <?php $upcoming = new WP_Query( array(
		'post_type' => Tribe__Events__Main::POSTTYPE,	) );
    while ( $upcoming->have_posts() ) {
$upcoming->the_post();
$title = get_the_title();
$date  = tribe_get_start_date($event_id, true, 'j M, y' );?>
<div class="post-wrap">
<p class="post_date"><?php echo $date ;?></p>
<p class="post_title"><?php echo $title ;?></p>
<div class="div_separator"></div>
</div>
<?php }?>


            </div>
          </div>
            <div class="col-6 social">
                <div class="social-list">
              <div class="ban-title"><h4>AQUALAND ON SOCIAL</h4></div>
              <div class="social-button"><a href="<?php echo wp_logout_url(home_url( '/media-social' )); ?>">See More</a>

            </div>
						<?php juicer_feed("name=aqualandaus-bf0dda37-84ed-4c61-85e0-11d3d98aa696&per=5"); ?>
          </div>


          </div>
        </div>
  </section>


					<?php if( get_field('gha_builder_area_additional_information') ) { ?>
						<div class="additional-info p-4 bg-white">
							<?php the_field('gha_builder_area_additional_information'); ?>
						</div>
					<?php } ?>
				</div>
			</div>


		</div>
	</div>
</div>
<style>
.builder-portal-header .search-form input.form-control {
    height: 60px;
  	  text-indent: 25px;
    font-size: 12px;
    color: #999999;
    border-radius: 4px;
    border:none;
    width: 100%;
}
.builder-portal-header{
	background:#fff;
	    height: 60px;
}
.builder-potal-body{
	margin-top:30px;
}

body{
	margin-top:-30px;
}
.menu-small{
	background-color:#274279 !important;
	min-height: 100%;
}
.portal-menu .menu li a {
    display: flex;
    width: 100%;
    padding: 20px 15px 20px 15px;
    margin: auto 0;
    font-family: 'Lato', sans-serif;
    font-size: 16px;
    color: #fff;
}
.portal-menu .menu li.current-menu-item {
    /*background: #213766;*/
		  background:#1B2D52;
}
.logo-holder{
	padding:10px;
	overflow:hidden;


}
.logo-holder img{
	margin:auto;
	align-items: center;
}
.portal-menu .menu li:before {
    margin: auto;
    font-size: 24px;
    margin-left: 30px;
    color: #fff;
}
.ghaico-ghaico-cevron-down:before {
    content: "\e90b";
		color:#222;
}
.news_holder{
  overflow:hidden!important;
  height: 225px;


}
.news_holder img{
  height:100% !important;
  width:100% !important;
}
.news-list{
  max-width:1350px;
}
.container{
          margin:auto;
              margin-top: 50px;
}
.link-box{
   padding:10px !important;

}
.short_desc{
    padding:20px;

  background-color:#fff;

}
.category-news{

    max-width:830px;
    margin-right:0;
    margin-left:auto;
    margin-top:-50px;

    text-align: left;

}
.category-news button {
    background:none!important;
     border:none;
     padding:0!important;
     color:#274279;
    font-size:24px;
    font-weight:500;


     cursor:pointer;
}
.decorated{

    color:#274279;
     overflow: hidden;

 }
.decorated > span{
    position: relative;
    display: inline-block;
}
.news_title{
  font-size:24px;
  color:#274279;
  font-weight:400;
  line-height: 1 !important;
  margin-bottom:2px;
  padding-top:30px;
}
.post_title{
  font-size:14px;
  color:#274279;
  font-weight:600;
  line-height: 1 !important;
  margin-bottom:2px;
  padding-top:5px;
}
.post_tag{
  font-size:12px;
  color:#00AEEF;
  margin-bottom:2px;
  padding-top: 5px;
}
.post_date{
  font-size:12px;
  color:#9396A4;
  margin-bottom:2px;
  padding-top: 5px;
}
.news_info p{
  padding-top:20px;
  color:#4D5474;
  font-weight:300 !important;
  font-size:14px !important;
  line-height:2 !important;
  padding-bottom:20px;
}
.news_date{
  color:#70768F;
  font-size:12px;
  font-weight:400;
  text-transform:uppercase;

}
.read-more a{
  border:2px solid #0072BB;
  display:block;
  color:#0072BB;
  width:90px;
  padding:8px;

}
.read-more p{
  margin-top:0;
  margin-bottom:0;
  font-size:14px;
}
span.search-place-icon {
    position: absolute;
    padding: 0 10px;
    font-size: 20px;
    color: #999;
}
@media only screen
  and (min-device-width: 769px)

 {


  .mobile-head{
		display:none;

  }
}
.project-tiles .column a {
  height: 100%;
  width: 100%;
  display: block;
  position: relative;
  overflow: hidden;
}
.project-tiles .column a:hover .project-bg:before,
.project-tiles .column a:focus .project-bg:before,
.project-tiles .column a:active .project-bg:before {
  background: rgba(47,83,143,.8);

}
.project-tiles .project-bg {
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  height: 100%;
  width: 100%;
  position: relative;
  -webkit-transition: all .3s ease-in-out;
  -moz-transition: all .3s ease-in-out;
  -o-transition: all .3s ease-in-out;
  transition: all .3s ease-in-out;
}

.project-tiles .column a:hover .project-bg,
.project-tiles .column a:focus .project-bg,
.project-tiles .column a:active .project-bg {
  -webkit-transform: scale(1.1);
  -ms-transform: scale(1.1);
  transform: scale(1.1);
}

.project-tiles .project-bg:before {
  content: " ";
  position: absolute;
  width: 100%;
  height: 100%;
  top:0;
  left: 0;
  background: rgba(0,0,0,.5);
  -webkit-transition: all .3s ease-in-out;
  -moz-transition: all .3s ease-in-out;
  -o-transition: all .3s ease-in-out;
  transition: all .3s ease-in-out;
}

.project-tiles .project-info {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);
  -webkit-transform: translate(-50%,-50%);
  -moz-transform: translate(-50%,-50%);
  text-align: center;
}

.project-tiles .project-info .line {
  height: 1px;
  width: 100%;
  background: #ffffff;
  margin: 2em 0;
}

.project-tiles .project-info h2,
.project-tiles .project-info h3 {
  color: #ffffff;
  margin: 0;
  padding: 0;
}

.different-tile{
  height: 20%;
}
.column {
  -ms-flex-preferred-size: 0;
  flex-basis: 0;
  -ms-flex-positive: 1;
  flex-grow: 1;
  max-width: 100%;
  padding:20px;
}
.announcements, .events , .social{
  padding:20px;
}
.event-list,.announce-list,.social-list{
  background-color:#fff;
  padding:40px;
}
.announce-list .post-wrap .div_separator{
  border-bottom:1px solid #E3E4E9;
  margin-top: 20px;
margin-bottom: 20px;
}
.announce-list .post-wrap:last-child .div_separator
{
border-bottom:0px;
}
.event-list .post-wrap .div_separator{
  border-bottom:1px solid #E3E4E9;
  margin-top: 20px;
margin-bottom: 20px;
}
.event-list .post-wrap:last-child .div_separator
{
border-bottom:0px;
}

.social-button a{
  border: 2px solid #0072BB;
display: block;
color: #0072BB;
width: 80px;
padding: 8px;
font-size:13px;
text-align:center;
}
.juicer-feed h1.referral {

    display: none;

}
.portal-menu .menu li a:after {
display:none;
}
.swiper-container {
	width: 100%;
	height: 100%;
}
.swiper-slide {
	text-align: center;
	font-size: 18px;
	background: #fff;
	/* Center slide text vertically */
	display: -webkit-box;
	display: -ms-flexbox;
	display: -webkit-flex;
	display: flex;
	-webkit-box-pack: center;
	-ms-flex-pack: center;
	-webkit-justify-content: center;
	justify-content: center;
	-webkit-box-align: center;
	-ms-flex-align: center;
	-webkit-align-items: center;
	align-items: center;
}
.swiper-pagination {
	position: absolute;
	top: 10px;
	right: 10px;
	width: auto !important;
	left: auto !important;
	margin: 0;
}
.swiper-pagination-bullet {
	padding: 5px 10px;
	border-radius: 0;
	width: auto;
	height: 30px;
	text-align: center;
	line-height: 30px;
	font-size: 12px;
	color:#000;
	opacity: 1;
	background: rgba(0,0,0,0.2);
}
.swiper-pagination-bullet-active {
	color:#fff;
	background: #007aff;
}
#wonderpluginslider-1{
	width: 905px!important;
height: 390px !important;
}
.amazingslider-nav-1{
	width:475px !important;
}
.amazingslider-bullet-1{
	width:100% !important;
	height:96px !important;
	padding-top:20px!important;
}
.amazingslider-bullet-wrapper-1{
	height:390px !important;
}
.amazingslider-car-right-arrow-1{
	display:none !important;
}
.amazingslider-bullet-text-1{
	width:350px !important;
}



</style>
<script>

</script>
<?php
endwhile;
wp_footer();
