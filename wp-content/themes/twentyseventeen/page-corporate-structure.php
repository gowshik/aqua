<?php
/**
 * Template Name: Corporate Structure
 *
*/
//get_header();

wp_head();
while ( have_posts() ) : the_post();
?>
<?php $search1 = $_GET['search'];?>
<link href="https://file.myfontastic.com/5kX8TPLEh2hK7BdGxf37rg/icons.css" rel="stylesheet">
<div class="builder-portal-header mobile-head">
	<div class="container-fluid px-5">
		<div class="row position-relative">
			<div class="col-8 d-flex col-lg-3 col-xl-2 builder-portalh">
				<span class="d-flex d-lg-none my-auto text-white mr-2 ghaico-ghaico-portal-menu"></span>
				<h2 class="my-auto mx-0 text-white">Aqualand</h2>
			</div>

			<div class="col-6 d-none d-lg-flex col-lg-6 col-xl-8">
				<div class="portal-search w-100 my-auto">
					<?php get_search_form(); ?>
					
				</div>
			</div>

			<div class="col-3 d-none d-lg-flex col-lg-3 col-xl-2 builder-account">
				<div class="my-auto w-100 text-white text-right">
					<!--img src="<?php echo get_template_directory_uri() . '/assets/images/gha-icon-portal.png' ?>"-->
					<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>
				</div>
			</div>

			<div class="gha-builder-pdetails p-4 bg-white position-absolute">
				<p class="text-medium font-weight-bold">My details</p>
				<table class="mb-3">
					<tbody>
						<tr>
							<td class="py-1" style="width: 80px;">Phone</td>
							<td class="font-weight-bold">1300 724 661</td>
						</tr>

						<tr>
							<td class="py-1" style="width: 80px;">Email</td>
							<td class="font-weight-bold">support@greenhomesaustralia.com.au</td>
						</tr>

						<tr>
							<td class="py-1" style="width: 80px;">Address</td>
							<td class="font-weight-bold">125 Byng Street, Orange, NSW 2800</td>
						</tr>
					</tbody>
				</table>

				<p class="text-medium font-weight-bold">
					<a href="<?php echo wp_logout_url(home_url( '/builder-area' )); ?>">Logout <span class="ghaico-ghaico-sign-out"></span></a>
				</p>
			</div>

		</div>
	</div>
</div>

<div class="builder-potal-body">
	<div class="container-fluid">
		<div class="row">





			<div class="d-lg-block col-lg-2 portal-menu pt-4 px-0 bg-white menu-small">

				<div class="d-block  d-lg-none my-3 px-3 w-100">
					<div class="sm-bdetals" style="border-bottom: 1px solid #DDDDDD; padding-bottom: 1rem;">
						<div>
							<img class="mr-2" src="<?php echo get_template_directory_uri() . '/assets/images/gha-portal-icon-alt.png' ?>"> GHA Orange
							<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>
							<span class="icon-chevron-down"></span>

						</div>

						<div class="gha-builder-pdetails p-4">
							<p class="text-medium font-weight-bold">My details</p>
							<table class="mb-3">
								<tbody>
									<tr>
										<td class="py-1" style="width: 80px;">Phone</td>
										<td class="font-weight-bold">1300 724 661</td>
									</tr>

									<tr>
										<td class="py-1" style="width: 80px;">Email</td>
										<td class="font-weight-bold">support@greenhomesaustralia.com.au</td>
									</tr>

									<tr>
										<td class="py-1" style="width: 80px;">Address</td>
										<td class="font-weight-bold">125 Byng Street, Orange, NSW 2800</td>
									</tr>
								</tbody>
							</table>

							<p class="text-medium font-weight-bold">
								<a href="#">Logout <span class="ghaico-ghaico-sign-out"></span></a>
							</p>
						</div>

					</div>
				</div>
				<div class="logo-holder" style="padding-bottom:30px;">
					<center><img src="<?php echo get_template_directory_uri() . '/assets/images/logo-white.png'; ?>"></center>
				</div>

				<?php
					wp_nav_menu( array(
					    'theme_location' => 'builder_area',
					) );
				?>
			</div>

			<div class=" col-lg-10 " style="margin-bottom: 80px;padding-right:5px;padding-left:2px">
				<div class="builder-portal-header desktop-head">
				<div class="row position-relative">


					<div class="col-10 d-none d-lg-flex col-lg-10 ">
						<div class="portal-search w-100 my-auto">
							<?php get_search_form(); ?>
						</div>
					</div>

					<div class="col-2 d-none d-lg-flex col-lg-2  builder-account">
						<div class="my-auto w-100 text-white text-right">

							<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>

						</div>
					</div>

					<div class="gha-builder-pdetails p-4 bg-white position-absolute">
						<p class="text-medium font-weight-bold" style="color:#274279;">My details</p>
						<table class="mb-3">
							<tbody>
								<tr>
									<td class="py-1" style="width: 80px;">Phone</td>
									<td class="font-weight-bold">02 9228 1888</td>
								</tr>

								<tr>
									<td class="py-1" style="width: 80px;">Email</td>
									<td class="font-weight-bold">info@aqualand.com.au</td>
								</tr>

								<tr>
									<td class="py-1" style="width: 80px;">Address</td>
									<td class="font-weight-bold">Tower One, Level 47,<br> 100 Barangaroo Avenue, <br>Barangaroo NSW 2000</td>
								</tr>
							</tbody>
						</table>

						<p class="text-medium font-weight-bold">
							<a style="color:#274279;" href="<?php echo wp_logout_url(home_url( '/aqualand' )); ?>">Logout <span class="ghaico-ghaico-sign-out"></span></a>
						</p>
					</div>
				</div>
				</div>

				<div class="builder-portal-resources">
					<ul>
						<?php
							if( have_rows('gha_builder_area_resources') ) :
								while ( have_rows('gha_builder_area_resources') ) : the_row();
						?>
									<li class="mb-4 bg-white"><a class="d-block p-4 ghaico-ghaico-download" href="<?php the_sub_field('gha_builder_area_resource_link'); ?>" target="_blank"><?php the_sub_field('gha_builder_area_resource_title'); ?></a></li>
						<?php
								endwhile;
							endif;
						?>
					</ul>
          <section id="posts" style="padding-top:40px;padding-bottom:100px;">
						<div class="row">
							<div class="col-2" style="padding-right: 0px;padding-left: 25px;padding-top:10px;">
								<div class="search-container" style="">
			<form action="" style="max-width:100%">
				<span class="search-place-icon ghaico-ghaico-search" style="padding:4px 10px;font-size:14px;"></span>
			<input type="text" placeholder="Search by Keyword" name="search" style="width:100%;padding-left: 30px;border-radius: 10px;border: none;font-size:14px">
			<button id ="edit-custom-search-blocks"type="submit"><i class="fa fa-search" style="color:#092140"></i></button>
			</form>
			</div>
								<div class="division" style="padding:20px; background:#fff">
									<div>
		                <h3 class="decorated"><span>DIVISION</span></h3>
		              </div>
									<!--input type="checkbox" id="c1" name="cc" />
 									<label for="c1"><span></span>Sales</label>

 									<input type="checkbox" id="c2" name="cc" />
 									<label for="c2"><span></span>Marketing</label>
									<input type="checkbox" id="c3" name="cc" />
 									<label for="c3"><span></span>Design</label>
									<input type="checkbox" id="c4" name="cc" />
 									<label for="c4"><span></span>Operations</label>
									<input type="checkbox" id="c5" name="cc" />
 									<label for="c5"><span></span>Cost</label>
									<input type="checkbox" id="c6" name="cc" />
 									<label for="c6"><span></span>Accounts</label-->
									<label class="containers">Sales
  								<input type="checkbox">
  							<span class="checkmark"></span>
							</label>
							<label class="containers">Marketing
  						<input type="checkbox">
  					<span class="checkmark"></span>
					</label>
					<label class="containers">Design
  				<input type="checkbox">
  			<span class="checkmark"></span>
			</label>
			<label class="containers">Operations
  		<input type="checkbox">
  	<span class="checkmark"></span>
	</label>
	<label class="containers">Cost
	<input type="checkbox">
<span class="checkmark"></span>
</label>
<label class="containers">Accounts
<input type="checkbox">
<span class="checkmark"></span>
</label>

							</div>
							<div class="location" style="padding:20px; background:#fff; margin-top:20px" >
								<div>
									<h3 class="decorated"><span>LOCATION</span></h3>
								</div>
								<label class="containers">Head Office
								<input type="checkbox">
							<span class="checkmark"></span>
						</label>
						<label class="containers">Lavendar Bay
						<input type="checkbox">
					<span class="checkmark"></span>
				</label>
				<label class="containers">The Revy
				<input type="checkbox">
			<span class="checkmark"></span>
		</label>
		<label class="containers">The Heysen
		<input type="checkbox">
	<span class="checkmark"></span>
</label>
<label class="containers">Bellvue
<input type="checkbox">
<span class="checkmark"></span>
</label>
<label class="containers">Linfield Village
<input type="checkbox">
<span class="checkmark"></span>
</label>
								<!--input type="checkbox" id="c1" name="cc" />
								<label for="c1"><span></span>Head Office</label>

								<input type="checkbox" id="c2" name="cc" />
								<label for="c2"><span></span>Lavendar Bay</label>
								<input type="checkbox" id="c3" name="cc" />
								<label for="c3"><span></span>The Revy</label>
								<input type="checkbox" id="c4" name="cc" />
								<label for="c4"><span></span>The Heysen</label>
								<input type="checkbox" id="c5" name="cc" />
								<label for="c5"><span></span>Bellvue</label>
								<input type="checkbox" id="c6" name="cc" />
								<label for="c6"><span></span>Linfield Village</label-->

							</div>
						</div>
							<div class="col-10">
          <div class=" container project-menu project-tiles" style="max-width:100%">

            <table cellspacing="10">

              <?php //if ( function_exists( 'wpsabox_author_box' ) ) var_dump(wpsabox_author_box()); ?>
							<?php $args1 = array(
	'blog_id'      => $GLOBALS['blog_id'],
	'role'         => 'subscriber',
	'role__in'     => array(),
	'role__not_in' => array(),
	'meta_key'     => '',
	'meta_value'   => '',
	'meta_compare' => '',
	'meta_query'   => array(),
	'date_query'   => array(),
	'include'      => array(),
	'exclude'      => array(),
	'orderby'      => 'login',
	'order'        => 'ASC',
	'offset'       => '',
	'search'       => '',
	'number'       => '',
	'count_total'  => false,
	'fields'       => 'all',
	'who'          => '',
); ?>
              <?php $coauthors = get_users($args1); //var_dump($coauthors);
							?>
         <?php foreach( $coauthors as $coauthor ):

					  ?>

           <tr class="user_details" style="height:80px;">
             <td>
              <div class="people-image" >
								<img src="http://localhost/aq/wp-content/uploads/2018/10/image-10.png">
								<!--a href="<?php echo $coauthor->user_url; ?>">
                <?php
   $picture = $coauthor->profile_pictures;
   if ($picture) {
   echo wp_get_attachment_image($picture);
   }
   ?></a-->

            <?php //$userdata = get_userdata( $coauthor->ID ); ?>

          </div>
          </td>

             <td>
           <p style="font-size:12px;"><span style="font-size:16px;color:#4D5474;font-weight:600;"><?php echo $coauthor->display_name; ?></span></p>
         </td>
         <td>
           <p>Head of Sales and Marketing</p>
         </td>
         <td>
           <p>Head Office</p>
         </td>
         <td>
           <p>+61 2 5550 1113</p>
         </td>
         <td>
           <p>0491 570 158</p>
         </td>
         <td>
            <p> <?php echo $coauthor->user_email; ?><br><a href="" style="text-decoration:underline;"></a></p>
          </td>


     </tr>
     <tr style="background:#F2F2F2;height:20px;">
     </tr>
         <?php endforeach; ?>


</table>

          </div>
				</div>




  </section>


					<?php if( get_field('gha_builder_area_additional_information') ) { ?>
						<div class="additional-info p-4 bg-white">
							<?php the_field('gha_builder_area_additional_information'); ?>
						</div>
					<?php } ?>
				</div>
			</div>


		</div>
	</div>
</div>
<style>
.builder-portal-header .search-form input.form-control {
    height: 60px;
  	  text-indent: 25px;
    font-size: 12px;
    color: #999999;
    border-radius: 4px;
    border:none;
    width: 100%;
}
.builder-portal-header{
	background:#fff;
	    height: 60px;
}
.builder-potal-body{
	margin-top:30px;
}
body{
	margin-top:-30px;
}
.menu-small{
	background-color:#274279 !important;
	min-height: 100%;
}
.portal-menu .menu li a {
    display: flex;
    width: 100%;
    padding: 20px 15px 20px 15px;
    margin: auto 0;
    font-family: 'Lato', sans-serif;
    font-size: 16px;
    color: #fff;
}
.portal-menu .menu li.current-menu-item {
    /*background: #213766;*/
		  background:#1B2D52;
}
.logo-holder{
	padding:10px;
	overflow:hidden;


}
.logo-holder img{
	margin:auto;
	align-items: center;
}
.portal-menu .menu li:before {
    margin: auto;
    font-size: 24px;
    margin-left: 30px;
    color: #fff;
}
.ghaico-ghaico-cevron-down:before {
    content: "\e90b";
		color:#222;
}
.news_holder{
  overflow:hidden!important;
  height: 225px;


}
.news_holder img{
  height:100% !important;
  width:100% !important;
}
.news-list{
  max-width:1350px;
}
.container{
          margin:auto;
              margin-top: 50px;
}
.link-box{
   padding:10px !important;

}
.short_desc{
    padding:20px;

  background-color:#fff;

}
.category-news{

    max-width:830px;
    margin-right:0;
    margin-left:auto;
    margin-top:-50px;

    text-align: left;

}
.category-news button {
    background:none!important;
     border:none;
     padding:0!important;
     color:#274279;
    font-size:24px;
    font-weight:500;


     cursor:pointer;
}
.decorated{

    color:#274279;
     overflow: hidden;

 }
.decorated > span{
    position: relative;
    display: inline-block;
}
.news_title{
  font-size:24px;
  color:#274279;
  font-weight:400;
  line-height: 1 !important;
  margin-bottom:2px;
  padding-top:30px;
}
.post_title{
  font-size:14px;
  color:#274279;
  font-weight:600;
  line-height: 1 !important;
  margin-bottom:2px;
  padding-top:5px;
}
.post_tag{
  font-size:12px;
  color:#00AEEF;
  margin-bottom:2px;
  padding-top: 5px;
}
.post_date{
  font-size:12px;
  color:#9396A4;
  margin-bottom:2px;
  padding-top: 5px;
}
.news_info p{
  padding-top:20px;
  color:#4D5474;
  font-weight:300 !important;
  font-size:14px !important;
  line-height:2 !important;
  padding-bottom:20px;
}
.news_date{
  color:#70768F;
  font-size:12px;
  font-weight:400;
  text-transform:uppercase;

}
.read-more a{
  border:2px solid #0072BB;
  display:block;
  color:#0072BB;
  width:90px;
  padding:8px;

}
.read-more p{
  margin-top:0;
  margin-bottom:0;
  font-size:14px;
}
span.search-place-icon {
    position: absolute;
    padding: 0 10px;
    font-size: 20px;
    color: #999;
}
@media only screen
  and (min-device-width: 769px)

 {


  .mobile-head{
		display:none;

  }
}
.project-tiles .column a {
  height: 100%;
  width: 100%;
  display: block;
  position: relative;
  overflow: hidden;
}
.project-tiles .column a:hover .project-bg:before,
.project-tiles .column a:focus .project-bg:before,
.project-tiles .column a:active .project-bg:before {
  background: rgba(47,83,143,.8);

}
.project-tiles .project-bg {
  background-size: cover;
  background-repeat: no-repeat;
  background-position: center;
  height: 100%;
  width: 100%;
  position: relative;
  -webkit-transition: all .3s ease-in-out;
  -moz-transition: all .3s ease-in-out;
  -o-transition: all .3s ease-in-out;
  transition: all .3s ease-in-out;
}

.project-tiles .column a:hover .project-bg,
.project-tiles .column a:focus .project-bg,
.project-tiles .column a:active .project-bg {
  -webkit-transform: scale(1.1);
  -ms-transform: scale(1.1);
  transform: scale(1.1);
}

.project-tiles .project-bg:before {
  content: " ";
  position: absolute;
  width: 100%;
  height: 100%;
  top:0;
  left: 0;
  background: rgba(0,0,0,.5);
  -webkit-transition: all .3s ease-in-out;
  -moz-transition: all .3s ease-in-out;
  -o-transition: all .3s ease-in-out;
  transition: all .3s ease-in-out;
}

.project-tiles .project-info {
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%,-50%);
  -ms-transform: translate(-50%,-50%);
  -webkit-transform: translate(-50%,-50%);
  -moz-transform: translate(-50%,-50%);
  text-align: center;
}

.project-tiles .project-info .line {
  height: 1px;
  width: 100%;
  background: #ffffff;
  margin: 2em 0;
}

.project-tiles .project-info h2,
.project-tiles .project-info h3 {
  color: #ffffff;
  margin: 0;
  padding: 0;
}

.project-menu .row {
  height: 20%;
}
.column {
  -ms-flex-preferred-size: 0;
  flex-basis: 0;
  -ms-flex-positive: 1;
  flex-grow: 1;
  max-width: 100%;
  padding:20px;
}
.announcements, .events , .social{
  padding:20px;
}
.event-list,.announce-list,.social-list{
  background-color:#fff;
  padding:40px;
}
.announce-list .post-wrap .div_separator{
  border-bottom:1px solid #E3E4E9;
  margin-top: 20px;
margin-bottom: 20px;
}
.announce-list .post-wrap:last-child .div_separator
{
border-bottom:0px;
}
.event-list .post-wrap .div_separator{
  border-bottom:1px solid #E3E4E9;
  margin-top: 20px;
margin-bottom: 20px;
}
.event-list .post-wrap:last-child .div_separator
{
border-bottom:0px;
}

.social-button a{
  border: 2px solid #0072BB;
display: block;
color: #0072BB;
width: 80px;
padding: 8px;
font-size:13px;
text-align:center;
}
table {
    border-collapse: collapse;
    width: 100%;
}

.user_details td {
    text-align: left;
    padding: 8px;
    background:#fff;
		color:#70768F;
		font-size:14px;
		padding-left: 40px;

}


.user_details:nth-child(even){/*background-color: #f2f2f2*/}
.user_details{

  border-spacing: 5px;
  padding-top: .5em;
  padding-bottom: .5em;

}
.portal-menu .menu li a:after {
display:none;
}
input[type="checkbox"] {
    display:none;
}

input[type="checkbox"] + label {
    color:#4D5268;
		display:block;
}

input[type="checkbox"] + label span {
    display:inline-block;
    width:19px;
    height:19px;
    margin:-2px 10px 0 0;
    vertical-align:middle;
    background:white;
		border:1px solid #0072BB;
    cursor:pointer;
}

input[type="checkbox"]:checked + label span {
    background:url(https://s3-us-west-2.amazonaws.com/s.cdpn.io/210284/check_radio_sheet.png) -19px top no-repeat;
}
.containers {
    display: block;
    position: relative;
    padding-left: 35px;
    margin-bottom: 12px;
    cursor: pointer;
    font-size: 14px;
		color:#4D5268;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.containers{
	margin-top:20px;
}

/* Hide the browser's default checkbox */
.containers input {
    position: absolute;
    opacity: 0;
    cursor: pointer;
}

/* Create a custom checkbox */
.checkmark {
    position: absolute;
    top: 0;
    left: 0;
    height: 22px;
    width: 22px;
		border:1px solid #0072BB;
    background-color: #fff;
}

/* On mouse-over, add a grey background color */
.containers:hover input ~ .checkmark {
    background-color: #ccc;
}

/* When the checkbox is checked, add a blue background */
.containers input:checked ~ .checkmark {
    background-color: #2196F3;
}

/* Create the checkmark/indicator (hidden when not checked) */
.checkmark:after {
    content: "";
    position: absolute;
    display: none;
}

/* Show the checkmark when checked */
.containers input:checked ~ .checkmark:after {
    display: block;
}

/* Style the checkmark/indicator */
.containers .checkmark:after {
    left: 9px;
    top: 1px;
    width: 5px;
    height: 17px;
    border: solid white;
    border-width: 0 3px 3px 0;
    -webkit-transform: rotate(45deg);
    -ms-transform: rotate(45deg);
    transform: rotate(45deg);
}
#edit-custom-search-blocks {
      position: absolute;
      top: -999px;
      left: -999px;
      width: 0px;
      height: 0px;
}
.people-image img {
    border-radius: 50%;
    height: 52px;
    width: 52px;
    background-repeat: no-repeat;
    background-position: center center;
    background-size: auto;
}




</style>
<?php
endwhile;
wp_footer();
