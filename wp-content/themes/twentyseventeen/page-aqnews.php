<?php
/**
 * Template Name: AQ News
 *
*/
//get_header();
wp_head();
while ( have_posts() ) : the_post();
?>
<link href="https://file.myfontastic.com/5kX8TPLEh2hK7BdGxf37rg/icons.css" rel="stylesheet">
<div class="builder-portal-header mobile-head">
	<div class="container-fluid px-5">
		<div class="row position-relative">
			<div class="col-8 d-flex col-lg-3 col-xl-2 builder-portalh">
				<span class="d-flex d-lg-none my-auto text-white mr-2 ghaico-ghaico-portal-menu"></span>
				<h2 class="my-auto mx-0 text-white">Aqualand</h2>
			</div>

			<div class="col-6 d-none d-lg-flex col-lg-6 col-xl-8">
				<div class="portal-search w-100 my-auto">
					<?php get_search_form(); ?>
				</div>
			</div>

			<div class="col-3 d-none d-lg-flex col-lg-3 col-xl-2 builder-account">
				<div class="my-auto w-100 text-white text-right">
					<!--img src="<?php echo get_template_directory_uri() . '/assets/images/gha-icon-portal.png' ?>"-->
					<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>
				</div>
			</div>

			<div class="gha-builder-pdetails p-4 bg-white position-absolute">
				<p class="text-medium font-weight-bold">My details</p>
				<table class="mb-3">
					<tbody>
						<tr>
							<td class="py-1" style="width: 80px;">Phone</td>
							<td class="font-weight-bold">1300 724 661</td>
						</tr>

						<tr>
							<td class="py-1" style="width: 80px;">Email</td>
							<td class="font-weight-bold">support@greenhomesaustralia.com.au</td>
						</tr>

						<tr>
							<td class="py-1" style="width: 80px;">Address</td>
							<td class="font-weight-bold">125 Byng Street, Orange, NSW 2800</td>
						</tr>
					</tbody>
				</table>

				<p class="text-medium font-weight-bold">
					<a href="<?php echo wp_logout_url(home_url( '/builder-area' )); ?>">Logout <span class="ghaico-ghaico-sign-out"></span></a>
				</p>
			</div>

		</div>
	</div>
</div>

<div class="builder-potal-body">
	<div class="container-fluid">
		<div class="row">





			<div class="d-lg-block col-lg-2 portal-menu pt-4 px-0 bg-white menu-small">

				<!--div class="d-block  d-lg-none my-3 px-3 w-100">
					<div class="sm-bdetals" style="border-bottom: 1px solid #DDDDDD; padding-bottom: 1rem;">
						<div>
							<img class="mr-2" src="<?php echo get_template_directory_uri() . '/assets/images/gha-portal-icon-alt.png' ?>"> GHA Orange
							<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>
							<span class="icon-chevron-down"></span>

						</div>

						<div class="gha-builder-pdetails p-4">
							<p class="text-medium font-weight-bold">My details</p>
							<table class="mb-3">
								<tbody>
									<tr>
										<td class="py-1" style="width: 80px;">Phone</td>
										<td class="font-weight-bold">1300 724 661</td>
									</tr>

									<tr>
										<td class="py-1" style="width: 80px;">Email</td>
										<td class="font-weight-bold">support@greenhomesaustralia.com.au</td>
									</tr>

									<tr>
										<td class="py-1" style="width: 80px;">Address</td>
										<td class="font-weight-bold">125 Byng Street, Orange, NSW 2800</td>
									</tr>
								</tbody>
							</table>

							<p class="text-medium font-weight-bold">
								<a href="#">Logout <span class="ghaico-ghaico-sign-out"></span></a>
							</p>
						</div>

					</div>
				</div-->
				<div class="logo-holder" style="padding-bottom:30px;">
					<center><img src="<?php echo get_template_directory_uri() . '/assets/images/logo-white.png'; ?>"></center>
				</div>

				<?php
					wp_nav_menu( array(
					    'theme_location' => 'builder_area',
					) );
				?>
			</div>

			<div class=" col-lg-10 " style="margin-bottom: 80px;padding-right:5px;padding-left:2px">
				<div class="builder-portal-header desktop-head">
				<div class="row position-relative">


					<div class="col-10 d-none d-lg-flex col-lg-10 ">
						<div class="portal-search w-100 my-auto">
							<?php get_search_form(); ?>
						</div>
					</div>

					<div class="col-2 d-none d-lg-flex col-lg-2  builder-account">
						<div class="my-auto w-100 text-white text-right">

							<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>

						</div>
					</div>

					<div class="gha-builder-pdetails p-4 bg-white position-absolute">
						<p class="text-medium font-weight-bold" style="color:#274279;">My details</p>
						<table class="mb-3">
							<tbody>
								<tr>
									<td class="py-1" style="width: 80px;">Phone</td>
									<td class="font-weight-bold">02 9228 1888</td>
								</tr>

								<tr>
									<td class="py-1" style="width: 80px;">Email</td>
									<td class="font-weight-bold">info@aqualand.com.au</td>
								</tr>

								<tr>
									<td class="py-1" style="width: 80px;">Address</td>
									<td class="font-weight-bold">Tower One, Level 47,<br> 100 Barangaroo Avenue, <br>Barangaroo NSW 2000</td>
								</tr>
							</tbody>
						</table>

						<p class="text-medium font-weight-bold">
							<a style="color:#274279;" href="<?php echo wp_logout_url(home_url( '/aqualand' )); ?>">Logout <span class="ghaico-ghaico-sign-out"></span></a>
						</p>
					</div>
				</div>
				</div>

				<div class="builder-portal-resources">
					<ul>
						<?php
							if( have_rows('gha_builder_area_resources') ) :
								while ( have_rows('gha_builder_area_resources') ) : the_row();
						?>
									<li class="mb-4 bg-white"><a class="d-block p-4 ghaico-ghaico-download" href="<?php the_sub_field('gha_builder_area_resource_link'); ?>" target="_blank"><?php the_sub_field('gha_builder_area_resource_title'); ?></a></li>
						<?php
								endwhile;
							endif;
						?>
					</ul>
          <section id="posts" style="padding-top:40px;padding-bottom:100px;">
            <div class="container news-list" style="">
              <div>
                <h2 class="decorated"><span>AQ NEWS</span></h2>
              </div>
              <div class="row category-news">
                <div class="col"><form action="" method="get"><button><h5>ALL ARTICLES</h5></button></form></div>
                <div class="col"><form action="" method="get"><button><h5>AQ GROUP</h5></button></form></div>
                <div class="col"><form action="" method="get"><button><h5>AQ PROPERTY</h5></button></form></div>
                <div class="col"><form action="" method="get"><button><h5>AQ CAPITAL</h5></button></form></div>
              </div>
              <div class="row" style="padding-top:40px;">
                <div class="row no-gutters">
                  <div class="col-md-12">
                    <?php $args2 = array( 'order' => 'DESC','posts_per_page' => '9','post_type'=>'aqnews');?>
                    <?php $q =new WP_query ( $args2 );
                    if ($q->have_posts() ) : ?>
                      <div class="row no-gutters" id="news-box" style="border-top:none;">
                        <?php while ($q->have_posts()) : $q->the_post(); ?>
                          <div class="col-md-4 link-box" style="">
                            <?php $tags = wp_get_post_tags($post->ID);
                              //var_dump($tags);
                              $t=$tags[0]->name;
                              //var_dump($t); ?>
                              <?php //if ( has_post_thumbnail()) {
                              //$url = wp_get_attachment_url( get_post_thumbnail_id() );
                               ?>
                            <div class="news_holder"style=""><?php the_post_thumbnail(array(900,500));?></div>
                              <?php //} ?>

                                  <div class="short_desc">
                                    <div style="height:225px">
                                      <span class="news_date"><?php echo get_the_date('j M, y'); ?></span><span> | </span><span class="news_date"> SYDNEY </span>
                                      <h3 class="news_title">
																				<?php //echo wp_trim_words( get_the_title(), 4 );
																						echo get_the_title();
																				 ?>
																			</h3>
                                      <div class="news_info" style="padding-top:10px;"><?php the_excerpt();?></div>
                                    </div>
                                <div class="row">

                              <div class="col-6 read-more" style="text-align:left;">
                                  <a href="<?php the_permalink(); ?>"><p style="font-size:14px">Read more</p></a>
                              </div>
                          </div>

                        </div>



                      </div>

    <?php endwhile; ?>
</div>
</div>
</div>
</div>
</div>
  </section>
  <?php endif;  ?>

					<?php if( get_field('gha_builder_area_additional_information') ) { ?>
						<div class="additional-info p-4 bg-white">
							<?php the_field('gha_builder_area_additional_information'); ?>
						</div>
					<?php } ?>
				</div>
			</div>


		</div>
	</div>
</div>
<style>
.builder-portal-header .search-form input.form-control {
    height: 60px;
  	  text-indent: 25px;
    font-size: 12px;
    color: #999999;
    border-radius: 4px;
    border:none;
    width: 100%;
}



body{
	margin-top:-30px;
}
.menu-small{
	background-color:#274279 !important;
	min-height: 100%;
}
.portal-menu .menu li a {
    display: flex;
    width: 100%;
    padding: 20px 15px 20px 15px;
    margin: auto 0;
    font-family: 'Lato', sans-serif;
    font-size: 16px;
    color: #fff;
}
.portal-menu .menu li.current-menu-item {
    /*background: #213766;*/
		  background:#1B2D52;
}
.logo-holder{
	padding:10px;
	overflow:hidden;


}
.logo-holder img{
	margin:auto;
	align-items: center;
}
.portal-menu .menu li:before {
    margin: auto;
    font-size: 24px;
    margin-left: 30px;
    color: #fff;
}
.ghaico-ghaico-cevron-down:before {
    content: "\e90b";
		color:#222;
}
.news_holder{
  overflow:hidden!important;
  height: 225px;


}
.news_holder img{
  height:100% !important;
  width:100% !important;
}
.news-list{
  max-width:1350px;
}
.container{
          margin:auto;
}
.link-box{
   padding:10px !important;

}
.short_desc{
    padding:20px;

  background-color:#fff;

}
.category-news{

    max-width:830px;
    margin-right:0;
    margin-left:auto;
    margin-top:-50px;

    text-align: left;

}
.category-news button {
    background:none!important;
     border:none;
     padding:0!important;
     color:#274279;
    font-size:24px;
    font-weight:500;


     cursor:pointer;
}
.decorated{

    color:#274279;
     overflow: hidden;

 }
.decorated > span{
    position: relative;
    display: inline-block;
}
.news_title{
  font-size:26px;
  color:#274279;
  font-weight:400;
  line-height: 1 !important;
  margin-bottom:2px;
  padding-top:30px;
}
.news_info p{
  padding-top:20px;
  color:#4D5474;
  font-weight:300 !important;
  font-size:14px !important;
  line-height:2 !important;
  padding-bottom:20px;
}
.news_date{
  color:#70768F;
  font-size:12px;
  font-weight:400;
  text-transform:uppercase;

}
.read-more a{
  border:2px solid #0072BB;
  display:block;
  color:#0072BB;
  width:90px;
  padding:8px;

}
.read-more p{
  margin-top:0;
  margin-bottom:0;
  font-size:14px;
}
span.search-place-icon {
    position: absolute;
    padding: 0 10px;
    font-size: 20px;
    color: #999;
}
@media only screen
  and (min-device-width: 769px)

 {


	 .mobile-head{
	 	display:none;
	 }
	 .builder-portal-header{
	 	background:#fff;
	 	    height: 60px;
	 }
	 .builder-potal-body{
	 	margin-top:30px;
	 }
}
.portal-menu .menu li a:after {
display:none;
}
@media only screen
  and (max-device-width: 768px)
	{
		.builder-portal-header {
    background: #274279;

    margin-top: 30px;
}

	}

</style>
<?php
endwhile;
wp_footer();
