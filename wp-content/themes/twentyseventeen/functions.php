<?php
/**
 * Twenty Seventeen functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 */

/**
 * Twenty Seventeen only works in WordPress 4.7 or later.
 */
if ( version_compare( $GLOBALS['wp_version'], '4.7-alpha', '<' ) ) {
	require get_template_directory() . '/inc/back-compat.php';
	return;
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function twentyseventeen_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed at WordPress.org. See: https://translate.wordpress.org/projects/wp-themes/twentyseventeen
	 * If you're building a theme based on Twenty Seventeen, use a find and replace
	 * to change 'twentyseventeen' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'twentyseventeen' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'twentyseventeen-featured-image', 2000, 1200, true );

	add_image_size( 'twentyseventeen-thumbnail-avatar', 100, 100, true );

	// Set the default content width.
	$GLOBALS['content_width'] = 525;

	// This theme uses wp_nav_menu() in two locations.
	register_nav_menus( array(
		'top'    => __( 'Top Menu', 'twentyseventeen' ),
		'builder_area' => __( 'Builder Area (Please add class "d-flex flex-row" for all menu items in Builder Area)', 'twentyseventeen' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'search-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 *
	 * See: https://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
		'gallery',
		'audio',
	) );

	// Add theme support for Custom Logo.
	add_theme_support( 'custom-logo', array(
		'width'       => 250,
		'height'      => 250,
		'flex-width'  => true,
	) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

	/*
	 * This theme styles the visual editor to resemble the theme style,
	 * specifically font, colors, and column width.
 	 */
	add_editor_style( array( 'assets/css/editor-style.css', twentyseventeen_fonts_url() ) );

	// Define and register starter content to showcase the theme on new sites.
	$starter_content = array(
		'widgets' => array(
			// Place three core-defined widgets in the sidebar area.
			'sidebar-1' => array(
				'text_business_info',
				'search',
				'text_about',
			),

			// Add the core-defined business info widget to the footer 1 area.
			'sidebar-2' => array(
				'text_business_info',
			),

			// Put two core-defined widgets in the footer 2 area.
			'sidebar-3' => array(
				'text_about',
				'search',
			),
		),

		// Specify the core-defined pages to create and add custom thumbnails to some of them.
		'posts' => array(
			'home',
			'about' => array(
				'thumbnail' => '{{image-sandwich}}',
			),
			'contact' => array(
				'thumbnail' => '{{image-espresso}}',
			),
			'blog' => array(
				'thumbnail' => '{{image-coffee}}',
			),
			'homepage-section' => array(
				'thumbnail' => '{{image-espresso}}',
			),
		),

		// Create the custom image attachments used as post thumbnails for pages.
		'attachments' => array(
			'image-espresso' => array(
				'post_title' => _x( 'Espresso', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/espresso.jpg', // URL relative to the template directory.
			),
			'image-sandwich' => array(
				'post_title' => _x( 'Sandwich', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/sandwich.jpg',
			),
			'image-coffee' => array(
				'post_title' => _x( 'Coffee', 'Theme starter content', 'twentyseventeen' ),
				'file' => 'assets/images/coffee.jpg',
			),
		),

		// Default to a static front page and assign the front and posts pages.
		'options' => array(
			'show_on_front' => 'page',
			'page_on_front' => '{{home}}',
			'page_for_posts' => '{{blog}}',
		),

		// Set the front page section theme mods to the IDs of the core-registered pages.
		'theme_mods' => array(
			'panel_1' => '{{homepage-section}}',
			'panel_2' => '{{about}}',
			'panel_3' => '{{blog}}',
			'panel_4' => '{{contact}}',
		),

		// Set up nav menus for each of the two areas registered in the theme.
		'nav_menus' => array(
			// Assign a menu to the "top" location.
			'top' => array(
				'name' => __( 'Top Menu', 'twentyseventeen' ),
				'items' => array(
					'link_home', // Note that the core "home" page is actually a link in case a static front page is not used.
					'page_about',
					'page_blog',
					'page_contact',
				),
			),

			// Assign a menu to the "social" location.
			'social' => array(
				'name' => __( 'Social Links Menu', 'twentyseventeen' ),
				'items' => array(
					'link_yelp',
					'link_facebook',
					'link_twitter',
					'link_instagram',
					'link_email',
				),
			),
		),
	);

	/**
	 * Filters Twenty Seventeen array of starter content.
	 *
	 * @since Twenty Seventeen 1.1
	 *
	 * @param array $starter_content Array of starter content.
	 */
	$starter_content = apply_filters( 'twentyseventeen_starter_content', $starter_content );

	add_theme_support( 'starter-content', $starter_content );
}
add_action( 'after_setup_theme', 'twentyseventeen_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function twentyseventeen_content_width() {

	$content_width = $GLOBALS['content_width'];

	// Get layout.
	$page_layout = get_theme_mod( 'page_layout' );

	// Check if layout is one column.
	if ( 'one-column' === $page_layout ) {
		if ( twentyseventeen_is_frontpage() ) {
			$content_width = 644;
		} elseif ( is_page() ) {
			$content_width = 740;
		}
	}

	// Check if is single post and there is no sidebar.
	if ( is_single() && ! is_active_sidebar( 'sidebar-1' ) ) {
		$content_width = 740;
	}

	/**
	 * Filter Twenty Seventeen content width of the theme.
	 *
	 * @since Twenty Seventeen 1.0
	 *
	 * @param int $content_width Content width in pixels.
	 */
	$GLOBALS['content_width'] = apply_filters( 'twentyseventeen_content_width', $content_width );
}
add_action( 'template_redirect', 'twentyseventeen_content_width', 0 );

/**
 * Register custom fonts.
 */
function twentyseventeen_fonts_url() {
	$fonts_url = '';

	/*
	 * Translators: If there are characters in your language that are not
	 * supported by Libre Franklin, translate this to 'off'. Do not translate
	 * into your own language.
	 */
	$lato = _x( 'on', 'Lato font: on or off', 'twentyseventeen' );

	if ( 'off' !== $lato ) {
		$font_families = array();

		$font_families[] = 'Lato:300,400,700';

		$query_args = array(
			'family' => urlencode( implode( '|', $font_families ) ),
			'subset' => urlencode( 'latin,latin-ext' ),
		);

		$fonts_url = add_query_arg( $query_args, 'https://fonts.googleapis.com/css' );
	}

	return esc_url_raw( $fonts_url );
}

/**
 * Add preconnect for Google Fonts.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array  $urls           URLs to print for resource hints.
 * @param string $relation_type  The relation type the URLs are printed.
 * @return array $urls           URLs to print for resource hints.
 */
function twentyseventeen_resource_hints( $urls, $relation_type ) {
	if ( wp_style_is( 'twentyseventeen-fonts', 'queue' ) && 'preconnect' === $relation_type ) {
		$urls[] = array(
			'href' => 'https://fonts.gstatic.com',
			'crossorigin',
		);
	}

	return $urls;
}
add_filter( 'wp_resource_hints', 'twentyseventeen_resource_hints', 10, 2 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function twentyseventeen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Blog Sidebar', 'twentyseventeen' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Add widgets here to appear in your sidebar on blog posts and archive pages.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 1', 'twentyseventeen' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );

	register_sidebar( array(
		'name'          => __( 'Footer 2', 'twentyseventeen' ),
		'id'            => 'sidebar-3',
		'description'   => __( 'Add widgets here to appear in your footer.', 'twentyseventeen' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'twentyseventeen_widgets_init' );

/**
 * Replaces "[...]" (appended to automatically generated excerpts) with ... and
 * a 'Continue reading' link.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $link Link to single post/page.
 * @return string 'Continue reading' link prepended with an ellipsis.
 */
function twentyseventeen_excerpt_more( $link ) {
	if ( is_admin() ) {
		return $link;
	}

	$link = sprintf( '<p class="link-more"><a href="%1$s" class="more-link">%2$s</a></p>',
		esc_url( get_permalink( get_the_ID() ) ),
		/* translators: %s: Name of current post */
		sprintf( __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'twentyseventeen' ), get_the_title( get_the_ID() ) )
	);
	return ' &hellip; ' . $link;
}
add_filter( 'excerpt_more', 'twentyseventeen_excerpt_more' );

/**
 * Handles JavaScript detection.
 *
 * Adds a `js` class to the root `<html>` element when JavaScript is detected.
 *
 * @since Twenty Seventeen 1.0
 */
function twentyseventeen_javascript_detection() {
	echo "<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>\n";
}
add_action( 'wp_head', 'twentyseventeen_javascript_detection', 0 );

/**
 * Add a pingback url auto-discovery header for singularly identifiable articles.
 */
function twentyseventeen_pingback_header() {
	if ( is_singular() && pings_open() ) {
		printf( '<link rel="pingback" href="%s">' . "\n", get_bloginfo( 'pingback_url' ) );
	}
}
add_action( 'wp_head', 'twentyseventeen_pingback_header' );

/**
 * Display custom color CSS.
 */
function twentyseventeen_colors_css_wrap() {
	if ( 'custom' !== get_theme_mod( 'colorscheme' ) && ! is_customize_preview() ) {
		return;
	}

	require_once( get_parent_theme_file_path( '/inc/color-patterns.php' ) );
	$hue = absint( get_theme_mod( 'colorscheme_hue', 250 ) );
?>
	<style type="text/css" id="custom-theme-colors" <?php if ( is_customize_preview() ) { echo 'data-hue="' . $hue . '"'; } ?>>
		<?php echo twentyseventeen_custom_colors_css(); ?>
	</style>
<?php }
add_action( 'wp_head', 'twentyseventeen_colors_css_wrap' );

/**
 * Enqueue scripts and styles.
 */
function twentyseventeen_scripts() {
	$my_theme = wp_get_theme()->get('Version');
	// Add custom fonts, used in the main stylesheet.
	wp_enqueue_style( 'twentyseventeen-fonts', twentyseventeen_fonts_url(), array(), null );

	wp_enqueue_style( 'bootstrap_css', get_template_directory_uri() . '/css/bootstrap.min.css' );
	wp_enqueue_style( 'number-css', get_template_directory_uri() . '/css/number.css', array(), $my_theme, 'all' );
	// icon-font
	wp_enqueue_style( 'gha-icon-font', get_template_directory_uri() . '/assets/font-icon/style.css', array(), $my_theme, 'all' );
	// slick slider csss
	wp_enqueue_style( 'gha-slick-css', get_template_directory_uri() . '/assets/css/slick.css', array(), $my_theme, 'all' );


	if( is_page_template('displayhomespage.php') ) {
		// fancybox-library
		wp_enqueue_style( 'gha-fancy-box', get_template_directory_uri() . '/assets/css/jquery.fancybox.css', array(), $my_theme, 'all' );
		wp_enqueue_script( 'twentyseventeen-fancybox', get_theme_file_uri( '/assets/js/jquery.fancybox.js' ), array( 'jquery' ), '3.3.5', true );

	}
	wp_enqueue_style( 'gha-slick-theme-css', get_template_directory_uri() . '/assets/css/slick-theme.css', array(), $my_theme, 'all' );
	// Theme stylesheet.
	wp_enqueue_style( 'twentyseventeen-style', get_stylesheet_uri(), array(), $my_theme, 'all' );

	// Load the dark colorscheme.
	if ( 'dark' === get_theme_mod( 'colorscheme', 'light' ) || is_customize_preview() ) {
		wp_enqueue_style( 'twentyseventeen-colors-dark', get_theme_file_uri( '/assets/css/colors-dark.css' ), array( 'twentyseventeen-style' ), '1.0' );
	}

	// Load the Internet Explorer 9 specific stylesheet, to fix display issues in the Customizer.
	if ( is_customize_preview() ) {
		wp_enqueue_style( 'twentyseventeen-ie9', get_theme_file_uri( '/assets/css/ie9.css' ), array( 'twentyseventeen-style' ), '1.0' );
		wp_style_add_data( 'twentyseventeen-ie9', 'conditional', 'IE 9' );
	}

	// Load the Internet Explorer 8 specific stylesheet.
	wp_enqueue_style( 'twentyseventeen-ie8', get_theme_file_uri( '/assets/css/ie8.css' ), array( 'twentyseventeen-style' ), '1.0' );
	wp_style_add_data( 'twentyseventeen-ie8', 'conditional', 'lt IE 9' );

	// Load the Internet Explorer 7 and older font-icon stylesheet.
	wp_enqueue_style( 'twentyseventeen-ie7', get_theme_file_uri( '/assets/font-icon/ie7/ie7.css' ), array( 'twentyseventeen-style' ), $my_theme );
	wp_style_add_data( 'twentyseventeen-ie7', 'conditional', 'lt IE 8' );

	// Load the Internet Explorer 7 and older font-icon script.
	wp_enqueue_script( 'gha-font-icon-ie7-js', get_theme_file_uri( '/assets/font-icon/ie7/ie7.js' ), array(), $my_theme );
	wp_script_add_data( 'gha-font-icon-ie7-js', 'conditional', 'lt IE 8' );

	// Load the html5 shiv.
	wp_enqueue_script( 'html5', get_theme_file_uri( '/assets/js/html5.js' ), array(), '3.7.3' );
	wp_script_add_data( 'html5', 'conditional', 'lt IE 9' );

	wp_enqueue_script( 'twentyseventeen-skip-link-focus-fix', get_theme_file_uri( '/assets/js/skip-link-focus-fix.js' ), array(), '1.0', true );

	$twentyseventeen_l10n = array(
		'quote'          => twentyseventeen_get_svg( array( 'icon' => 'quote-right' ) ),
	);

	if ( has_nav_menu( 'top' ) ) {
		wp_enqueue_script( 'twentyseventeen-navigation', get_theme_file_uri( '/assets/js/navigation.js' ), array( 'jquery' ), '1.0', true );
		$twentyseventeen_l10n['expand']         = __( 'Expand child menu', 'twentyseventeen' );
		$twentyseventeen_l10n['collapse']       = __( 'Collapse child menu', 'twentyseventeen' );
		$twentyseventeen_l10n['icon']           = twentyseventeen_get_svg( array( 'icon' => 'angle-down', 'fallback' => true ) );
	}

	wp_enqueue_script( 'bootstrap_popover_js','https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js', array('jquery'), $my_theme, true );
	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/js/js/bootstrap.min.js', array('jquery'), '', true );
	wp_enqueue_script( 'twentyseventeen-global', get_theme_file_uri( '/assets/js/global.js' ), array( 'jquery' ), '1.0', true );
	wp_enqueue_script( 'twentyseventeen-hotspot', get_theme_file_uri( '/assets/js/jquery.hotspot.js' ), array( 'jquery' ), $my_theme, true );
	wp_enqueue_script( 'gha-slick-js', get_template_directory_uri() . '/assets/js/slick.js', array('jquery'), $my_theme, true);
	wp_enqueue_script( 'number-js', get_template_directory_uri() . '/js/js/number.js', array('jquery'), $my_theme, true);
	wp_enqueue_script( 'history-js', get_template_directory_uri() . '/assets/js/jquery.history.js', array('jquery'), $my_theme, true);

	wp_enqueue_script( 'jquery-scrollto', get_theme_file_uri( '/assets/js/jquery.scrollTo.js' ), array( 'jquery' ), '2.1.2', true );

	wp_localize_script( 'twentyseventeen-skip-link-focus-fix', 'twentyseventeenScreenReaderText', $twentyseventeen_l10n );


	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

	// register our main script but do not enqueue it yet
	// wp_register_script( 'my_loadmore', get_template_directory_uri() . '/assets/js/myloadmore.js', array('jquery') );

	// global $wp_query;

	// In most cases it is already included on the page and this line can be removed
	// wp_enqueue_script('jquery');

	// register our main script but do not enqueue it yet

	// $args['post_type']   = 'home';
	// 	$args['post_status'] = 'publish';

	// 	$loop  = new WP_Query( $args );
	// 	$max   = $loop->max_num_pages;
	// 	$args  = array(
	// 		'nonce' => wp_create_nonce( 'mb-load-more-nonce' ),
	// 		'url'   => admin_url( 'admin-ajax.php' ),
	// 		'posts_per_page' => get_option('posts_per_page'),
	// 		'max'   => $max,
	// 		'action' => 'mb_ajax_archive_load_more'
	// 	);
	// wp_enqueue_script( 'my_loadmore', get_template_directory_uri() . '/assets/js/myloadmore.js', array('jquery') );
	// wp_localize_script( 'my_loadmore', 'mbloadmorearchive', $args );
}
add_action( 'wp_enqueue_scripts', 'twentyseventeen_scripts' );

// enqueue font-icon style to admin
function load_custom_font_icon_wp_admin_style() {
		$my_theme = wp_get_theme()->get('Version');
        wp_enqueue_style( 'gha-admin-icon-font', get_template_directory_uri() . '/assets/font-icon/style.css', array(), $my_theme, 'all' );
}
add_action( 'admin_enqueue_scripts', 'load_custom_font_icon_wp_admin_style' );
/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for content images.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $sizes A source size value for use in a 'sizes' attribute.
 * @param array  $size  Image size. Accepts an array of width and height
 *                      values in pixels (in that order).
 * @return string A source size value for use in a content image 'sizes' attribute.
 */
function twentyseventeen_content_image_sizes_attr( $sizes, $size ) {
	$width = $size[0];

	if ( 740 <= $width ) {
		$sizes = '(max-width: 706px) 89vw, (max-width: 767px) 82vw, 740px';
	}

	if ( is_active_sidebar( 'sidebar-1' ) || is_archive() || is_search() || is_home() || is_page() ) {
		if ( ! ( is_page() && 'one-column' === get_theme_mod( 'page_options' ) ) && 767 <= $width ) {
			 $sizes = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
		}
	}

	return $sizes;
}
add_filter( 'wp_calculate_image_sizes', 'twentyseventeen_content_image_sizes_attr', 10, 2 );

/**
 * Filter the `sizes` value in the header image markup.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $html   The HTML image tag markup being filtered.
 * @param object $header The custom header object returned by 'get_custom_header()'.
 * @param array  $attr   Array of the attributes for the image tag.
 * @return string The filtered header image HTML.
 */
function twentyseventeen_header_image_tag( $html, $header, $attr ) {
	if ( isset( $attr['sizes'] ) ) {
		$html = str_replace( $attr['sizes'], '100vw', $html );
	}
	return $html;
}
add_filter( 'get_header_image_tag', 'twentyseventeen_header_image_tag', 10, 3 );

/**
 * Add custom image sizes attribute to enhance responsive image functionality
 * for post thumbnails.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param array $attr       Attributes for the image markup.
 * @param int   $attachment Image attachment ID.
 * @param array $size       Registered image size or flat array of height and width dimensions.
 * @return array The filtered attributes for the image markup.
 */
function twentyseventeen_post_thumbnail_sizes_attr( $attr, $attachment, $size ) {
	if ( is_archive() || is_search() || is_home() ) {
		$attr['sizes'] = '(max-width: 767px) 89vw, (max-width: 1000px) 54vw, (max-width: 1071px) 543px, 580px';
	} else {
		$attr['sizes'] = '100vw';
	}

	return $attr;
}
add_filter( 'wp_get_attachment_image_attributes', 'twentyseventeen_post_thumbnail_sizes_attr', 10, 3 );

/**
 * Use front-page.php when Front page displays is set to a static page.
 *
 * @since Twenty Seventeen 1.0
 *
 * @param string $template front-page.php.
 *
 * @return string The template to be used: blank if is_home() is true (defaults to index.php), else $template.
 */
function twentyseventeen_front_page_template( $template ) {
	return is_home() ? '' : $template;
}
add_filter( 'frontpage_template',  'twentyseventeen_front_page_template' );

/**
 * Modifies tag cloud widget arguments to display all tags in the same font size
 * and use list format for better accessibility.
 *
 * @since Twenty Seventeen 1.4
 *
 * @param array $args Arguments for tag cloud widget.
 * @return array The filtered arguments for tag cloud widget.
 */
function twentyseventeen_widget_tag_cloud_args( $args ) {
	$args['largest']  = 1;
	$args['smallest'] = 1;
	$args['unit']     = 'em';
	$args['format']   = 'list';

	return $args;
}
add_filter( 'widget_tag_cloud_args', 'twentyseventeen_widget_tag_cloud_args' );

/**
 * Implement the Custom Header feature.
 */
require get_parent_theme_file_path( '/inc/custom-header.php' );

/**
 * Custom template tags for this theme.
 */
require get_parent_theme_file_path( '/inc/template-tags.php' );

/**
 * Additional features to allow styling of the templates.
 */
require get_parent_theme_file_path( '/inc/template-functions.php' );

/**
 * Customizer additions.
 */
require get_parent_theme_file_path( '/inc/customizer.php' );

/**
 * SVG icons functions and filters.
 */
require get_parent_theme_file_path( '/inc/icon-functions.php' );

// add_action( 'wp_enqueue_scripts', 'theme_styles');

// function theme_js() {

// 	global $wp_scripts;

// 	wp_enqueue_script( 'bootstrap_js', get_template_directory_uri() . '/js/js/bootstrap.min.js' );
// 	wp_enqueue_script( 'number-js', get_template_directory_uri() . '/js/js/number.js');

// }

// add_action( 'wp_enqueue_scripts', 'theme_js');

if( !function_exists('base_get_all_custom_fields') ) {
	function base_get_all_custom_fields(){
		global $post;
		global $wpdb;
		$sql = "SELECT * FROM $wpdb->postmeta	WHERE post_id = $post->ID ORDER BY meta_id ASC";
		$custom_fields = $wpdb->get_results($sql);
		$custom_field_array = array();
		foreach($custom_fields as $field) {
			$custom_field_array["$field->meta_key"] = $field->meta_value;
		}
		return $custom_field_array;
	}
}

// build green timeline
function gha_buildgreen_timeline() {
	?>
		<div class="benefit-timeline">
	    	<div class="container">
				<div class="timeline">
					<div class="timeline-wrapper tmlr">
						<div class="timeline-image"><img src="<?php echo get_template_directory_uri() . '/assets/images/timeline/benefits-1-efficiencies.svg' ?>"></div>
						<div class="timeline-container right">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/timeline/benefits-1-efficiencies.svg' ?>">
							<div class="content">
							  <p>Better efficiencies mean lower energy costs</p>
							  <a tabindex="0" class="tml-popover" data-toggle="bs_popover" data-trigger="focus" title="Better efficiencies mean lower energy costs" data-placement="top" data-content="An energy efficient home drastically reduces your energy costs, because every outlet and appliance is fine-tuned to get the very best value from the power it uses.">More Info</a>

							</div>
						</div>
					</div>

					<div class="timeline-wrapper tmll">
						<div class="timeline-image"><img src="<?php echo get_template_directory_uri() . '/assets/images/timeline/benefits-2-comfort.svg' ?>"></div>
						<div class="timeline-container left">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/timeline/benefits-2-comfort.svg' ?>">
							<div class="content">
							  <p>More home comfort, all year long</p>
							  <a tabindex="0" class="tml-popover" data-toggle="bs_popover" data-trigger="focus" title="More home comfort, all year long" data-placement="top" data-content="With enhanced efficiency comes enhanced comfort and control of your home environment. Hot or cold, the happy medium is easier to maintain throughout all seasons">More Info</a>
							</div>
						</div>
					</div>

					<div class="timeline-wrapper tmlr">
						<div class="timeline-image"><img src="<?php echo get_template_directory_uri() . '/assets/images/timeline/benefits-3-affordable.svg' ?>"></div>
						<div class="timeline-container right">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/timeline/benefits-3-affordable.svg' ?>">
						<div class="content">
						  <p>A sustainable, practical & affordable build</p>
						  <a tabindex="0" class="tml-popover" data-toggle="bs_popover" data-trigger="focus" title="A sustainable, practical & affordable build" data-placement="top" data-content="It’s a common mistake to think that building your Green home will be a complicated, expensive exercise. GHA has been doing this a long time, we’ll show you how easy it can be.">More Info</a>
						</div>
						</div>
					</div>

					<div class="timeline-wrapper tmll">
						<div class="timeline-image"><img src="<?php echo get_template_directory_uri() . '/assets/images/timeline/benefits-4-better.svg' ?>"></div>
						<div class="timeline-container left">
							<img src="<?php echo get_template_directory_uri() . '/assets/images/timeline/benefits-4-better.svg' ?>">
							<div class="content">
							  <p>Greater efficiencies lead to better homes</p>
							  <a tabindex="0" class="tml-popover" data-toggle="bs_popover" data-trigger="focus" title="Greater efficiencies lead to better homes" data-placement="top" data-content="GHA is the only builder in Australia to be certified Green by the International Standards Organisation (ISO), with nine independent inspections on each home.">More Info</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	<?php
}
// builder search form


// load  more home designs with ajax call

function gha_load_posts_by_ajax_callback() {
    check_ajax_referer('load_more_posts', 'security');
    $paged = $_POST['page'];
    $args = array(
        'post_type' => 'home',
        'post_status' => 'publish',
        'posts_per_page' => get_option('posts_per_page'),
        'orderby' => 'title',
		'order' => 'DESC',
        'paged' => $paged,
    );
    $my_posts = new WP_Query( $args );

    if ( $my_posts->have_posts() ) :
        ?>
        <?php while ( $my_posts->have_posts() ) : $my_posts->the_post() ?>
            <div class="home-design col-sm-6 col-md-4">
				<div class="home-design-image">
					<?php
						echo '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">';
				echo '<div class="overlay"></div>';
							if( has_post_thumbnail() ) {
								the_post_thumbnail('medium');
							}
							the_title( '<h3>', '</h3>' );
						echo '</a>';
					?>
				</div>
				<div class="design-meta">
					<div class="meta-in"><span class="mimg"><img src="<?php echo get_template_directory_uri();?>/assets/images/gha-area.svg"></span><span> <?php if( get_field('landarea') ) { echo get_field('landarea') . 'M<sup>2</sup>'; } else { echo 'NA'; } ?> </span></div>
					<div class="meta-in"><span class="mimg"><img src="<?php echo get_template_directory_uri();?>/assets/images/gha-bedrooms.svg"></span><span><?php if( get_field('bedrooms') ) { echo get_field('landarea'); } else { echo 'NA'; } ?></span></div>
					<div class="meta-in"><span class="mimg"><img src="<?php echo get_template_directory_uri();?>/assets/images/gha-bathrooms.svg"></span><span><?php if( get_field('landarea') ) { echo get_field('bathrooms'); } else { echo 'NA'; } ?></span></div>
					<div class="meta-in"><span class="mimg"><img src="<?php echo get_template_directory_uri();?>/assets/images/gha-carspaces.svg"></span><span><?php if( get_field('landarea') ) { echo get_field('carspaces'); } else { echo 'NA'; } ?></span></div>
				</div>
			</div>
        <?php endwhile ?>
        <?php
    endif;

    wp_die();
}
add_action('wp_ajax_load_posts_by_ajax', 'gha_load_posts_by_ajax_callback');
add_action('wp_ajax_nopriv_load_posts_by_ajax', 'gha_load_posts_by_ajax_callback');

// allow svg to upload via media uploader
function cc_mime_types($mimes) {
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// function showBeforeMore($fullText){
//     if(@strpos($fullText, '<!--more-->')){
//         $morePos = strpos($fullText, '<!--more-->');
//         print substr($fullText,0,$morePos);
//         print "<span class=\"read-more\"><a href=\"". get_permalink() . "\" class=\"read-more-link\">Read More</a></span>";
//     } else {
//         print $fullText;
//     }
// }

// function readmore($fullText){
// 	if(@strpos($fullText, '<!--more-->')){
// 		$morePos  = strpos($fullText, '<!--more-->');
// 		$fullText = preg_replace('/<!--(.|\s)*?-->/', '', $fullText);
// 		print substr($fullText,0,$morePos);
// 		print "<div class=\"read-more-content hide\">". substr($fullText,$morePos,-1) . "</div>";
// 		print "<a class=\"ui lined small button read-more\">Read More</a>";
// 	} else {
// 		print $fullText;
// 	}
// }

function my_acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyD9T6vYFqkZlm6SiQCdvA0W-ymeHLSV5XM';
	return $api;
}
add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');



// acf field to rest api
function home_get_post_meta_cb($object, $field_name, $request){
        return get_post_meta($object['id'], $field_name, true);
}

function home_update_post_meta_cb($value, $object, $field_name){
  return update_post_meta($object['id'], $field_name, $value);
}

add_action('rest_api_init', function(){

  register_rest_field('home', 'landarea',
    array(
    'get_callback' => 'home_get_post_meta_cb',
    'update_callback' => 'home_update_post_meta_cb',
    'schema' => null

    )
  );

  register_rest_field('home', 'bedrooms',
    array(
    'get_callback' => 'home_get_post_meta_cb',
    'update_callback' => 'home_update_post_meta_cb',
    'schema' => null

    )
  );

  register_rest_field('home', 'bathrooms',
    array(
    'get_callback' => 'home_get_post_meta_cb',
    'update_callback' => 'home_update_post_meta_cb',
    'schema' => null

    )
  );

  register_rest_field('home', 'carspaces',
    array(
    'get_callback' => 'home_get_post_meta_cb',
    'update_callback' => 'home_update_post_meta_cb',
    'schema' => null

    )
  );

});

// add support meta_query filter rest api v2
add_filter( 'rest_query_vars', 'test_query_vars' );
function test_query_vars ( $vars ) {
    $vars[] = 'meta_query';
    return $vars;
}

// remove_action( 'admin_init', 'send_frame_options_header', 10, 0 );
// remove_action( 'login_init', 'send_frame_options_header', 10, 0 );


add_action( 'init', 'codex_member_init' );
/**
 * Register a board_member post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_member_init() {
	$labels = array(
		'name'               => _x( 'Board Members', 'post type general name', 'your-plugin-textdomain' ),
		'singular_name'      => _x( 'member', 'post type singular name', 'your-plugin-textdomain' ),
		'menu_name'          => _x( 'Board Members', 'admin menu', 'your-plugin-textdomain' ),
		'name_admin_bar'     => _x( 'member', 'add new on admin bar', 'your-plugin-textdomain' ),
		'add_new'            => _x( 'Add New', 'board_member', 'your-plugin-textdomain' ),
		'add_new_item'       => __( 'Add New member', 'your-plugin-textdomain' ),
		'new_item'           => __( 'New Member', 'your-plugin-textdomain' ),
		'edit_item'          => __( 'Edit Member', 'your-plugin-textdomain' ),
		'view_item'          => __( 'View Member', 'your-plugin-textdomain' ),
		'all_items'          => __( 'All Board Members', 'your-plugin-textdomain' ),
		'search_items'       => __( 'Search Board Members', 'your-plugin-textdomain' ),
		'parent_item_colon'  => __( 'Parent Board Members:', 'your-plugin-textdomain' ),
		'not_found'          => __( 'No Board Members found.', 'your-plugin-textdomain' ),
		'not_found_in_trash' => __( 'No Board Members found in Trash.', 'your-plugin-textdomain' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Description.', 'your-plugin-textdomain' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'board-member' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' )
	);

	register_post_type( 'board_member', $args );
}




// if vistor is not logged in user, then redirect to login page if tried to access builder area page
add_action( 'template_redirect', 'redirect_to_specific_page' );

function redirect_to_specific_page() {

if ( is_page_template('page-template-builder-area.php') && ! is_user_logged_in() ) {

wp_redirect( "'" . esc_url( home_url( '/builder-area' ) ) . "'", 301 );
  exit;
    }
}
function word_count($string, $limit) {

$words = explode(' ', $string);

return implode(' ', array_slice($words, 0, $limit));

}
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
if (!current_user_can('administrator') && !is_admin()) {
  show_admin_bar(false);
}
}
function login_required() {
    global $post;
    $restricted = array( 1022, 1119, 1125, 1114, 1121, 1023, 1117 ); // all your restricted pages
    if( in_array( $post->ID, $restricted ) && ! is_user_logged_in() ) {
        wp_redirect( home_url('/') );
        exit();
    }
}
//add_action( 'template_redirect', 'login_required' );
