<?php
/**
 * Template Name: Aqualand login
 *
*/
wp_head();
while ( have_posts() ) : the_post();
?>
<div class="builder-portal-login" style="padding:200px">
	<div class="container-fluid px-5">
		<div style="max-width: 500px; margin: 0 auto; padding: 50px 0;">
		<div class="portal-login-description mb-4 text-center">
			<h4 class="mb-3" style="color: #FFF;">WELCOME TO AQUALAND</h4>

		</div>
		<div class="portal-login-form p-4 text-center">
			<img src="<?php echo get_template_directory_uri() . '/assets/images/logo-white.png'; ?>">
			 <?php
			 	$args = array(
					'echo'           => true,
					'remember'       => true,
					'redirect'       => site_url( 'aqualand/dashboard' ),
					'form_id'        => 'loginform',
					'id_username'    => 'user_login',
					'id_password'    => 'user_pass',
					'id_remember'    => 'rememberme',
					'id_submit'      => 'wp-submit',
					'label_username' => __( 'Username' ),
					'label_password' => __( 'Password' ),
					'label_remember' => __( 'Remember Me' ),
					'label_log_in'   => __( 'Log In' ),
					'value_username' => '',
					'value_remember' => false
				);
			 	wp_login_form( $args );
			?>
		</div>
		<div>
			<a href="http://www.aqualand.com.au/"><p style="color:#fff; font-size:14px;padding-top:10px;"> &larr; Back to Aqualand Website </p></a>
		</div>
	</div>
</div>
</div>
<style>
body{
	background:#274279;
	margin-top:-30px;
}
.portal-login-form input[type="submit"] {
    background: #0072BB;
	}
	.portal-login-form input {
    padding: 0 0.5rem;

}
.portal-login-form label{
	color:#fff;
}
</style>
<?php
endwhile;
