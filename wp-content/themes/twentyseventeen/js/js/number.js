(function( $ ) {

    $.fn.number = function(customOptions) {

        var options = {

            'containerClass' : 'number-style',
            'minus' : 'number-minus',
            'plus' : 'number-plus',
            'containerTag' : 'div',
            'btnTag' : 'span'

        };

        options = $.extend(true, options, customOptions);

        var input = this;

        input.wrap('<' + options.containerTag + ' class="' + options.containerClass + '">');

        var wrapper = input.parent();

        wrapper.prepend('<' + options.btnTag + ' class="' + options.minus + '"></' + options.btnTag + '>');

        var minus = wrapper.find('.' + options.minus);

        wrapper.append('<span>+</span><' + options.btnTag + ' class="' + options.plus + '"></' + options.btnTag + '>');

        var plus = wrapper.find('.' + options.plus);

        var min = input.attr('min');

        var max = input.attr('max');

        if(input.attr('step')){

            var step = +input.attr('step');

        } else {

            var step = 1;

        }

        if(+input.val() <= +min){

            minus.addClass('disabled');

        }

        if(+input.val() >= +max){

            plus.addClass('disabled');

        }

        minus.click(function () {

            var input = $(this).parent().find('input');
            var inputEm = $(this).parent().find('input + span');
            // console.log(inputEm);

            var value = input.val();

            if(+value > +min){

                input.val(+value - step);

                if(+input.val() === +min){

                    input.prev('.' + options.minus).addClass('disabled');

                }

                if(inputEm.next('.' + options.plus).hasClass('disabled')){

                    inputEm.next('.' + options.plus).removeClass('disabled')

                }

            } else if(!min){

                input.val(+value - step);

            }
            // console.log(input.val());

        });

        plus.click(function () {

            var input = $(this).parent().find('input');
            var inputEm = $(this).parent().find('input + span');

            var value = input.val();

            if(+value < +max){

                input.val(+value + step);

                if(+input.val() === +max){

                    inputEm.next('.' + options.plus).addClass('disabled');

                }

                if(input.prev('.' + options.minus).hasClass('disabled')){

                    input.prev('.' + options.minus).removeClass('disabled')

                }

            } else if(!max){

                input.val(+value + step);

            }

            // console.log(input.val());
        });

    };

    // calculate height of banner and align inner banner div vertically center
    var bannerHeight = jQuery('.banner-inner').outerHeight(true);
    jQuery('.banner-inner').css('margin-top', -bannerHeight/2)

    var bannerHeighta = jQuery('#section-6 .banner-inner').outerHeight();
    // console.log(bannerHeighta);
    jQuery('#section-6 .banner-inner').css('margin-top', -bannerHeighta/2)

// calculate height of tab content container and align inner banner div vertically center
    var tabContentHeight = jQuery('.gha-b-process-content').outerHeight(true);
    jQuery('.gha-b-process-content').css('margin-top', -tabContentHeight/2)

    // calculate height of page-banner-search and align middle of bottom
    var sh = jQuery('.page-banner-search').outerHeight(true);
    jQuery('.page-banner-search').css('bottom', -sh/2)

    jQuery('.show-more a').click(function() {
        $('#hd-design').toggleClass('show');
    });

    // popover for timeline
    jQuery('.tml-popover').popover({
      trigger: 'focus hover'
    });

    // faq accordian
    jQuery( function() {
        jQuery( ".hd-faq-accordian" ).accordion({
            collapsible: true,
            active: false
        });
    } );

    // modal video for featured images
    $(".videoModal").on('hidden.bs.modal', function (e) {
        $('.modal-vid').trigger('pause');
    });

    $(".videoModal").on('shown.bs.modal', function (e) {
        $('.modal-vid').trigger('play');
    });

    $(".cust_testi").on('hidden.bs.modal', function (e) {
        $(".cust_testi iframe").attr("src", $(".cust_testi iframe").attr("src"));
    });

     $(".videoModal").on('hidden.bs.modal', function (e) {
        $(".videoModal iframe").attr("src", $(".videoModal iframe").attr("src"));
    });


    // design home list search
    // change input field style
    $('.dh-search-wrapp input[type="number"]').each(function () {
        $(this).number();
    });

    // toggle class in homepage scetion 3
    $( ".list-wrapper" ).click(function() {
      $( this ).toggleClass( "display" );
      $( ".list-wrapper" ).not(this).removeClass("display");
    });

    // toggle menu class in builder-portal
    $( ".builder-portalh" ).click(function() {
      $( ".builder-potal-body" ).toggleClass( "display" );
      $(".menu-small").toggle("slow");
      // $( ".builder-portalh" ).not(this).removeClass("display");
    });

    // toggle builder details in builder-portal small screen
    jQuery( ".sm-bdetals" ).click(function() {
      jQuery( ".gha-builder-pdetails" ).toggleClass( "display" );
    });

    // slick slider
      $('.home-featured-slider').slick({
        dots: false,
        infinite: false,
        speed: 300,
        slidesToShow: 3,
        slidesToScroll: 3,
        responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        ]
      });

// hotSpot image
    if (jQuery('#hotspotImg').length > 0) {
            jQuery('#hotspotImg').hotSpot({
        bindselector: 'click'
      });
    }

    var _gaq = _gaq || [];
    _gaq.push(['_setAccount', 'UA-36251023-1']);
    _gaq.push(['_setDomainName', 'jqueryscript.net']);
    _gaq.push(['_trackPageview']);

    (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
    })();

// hotspot image close
    jQuery('.tab-next').click(function() {
        var href = jQuery(this).attr('href');
    // console.log(href);
        jQuery('a[href="'+href+'"]').tab('show')
    });

    // toggle filter box for mobile, homedesign template
    $( "#mobile-filter-toggle" ).click(function() {
      $( ".dh-search-wrapp form" ).toggle();
       $( ".dh-search-wrapp" ).toggleClass('m-filter');
       var x = document.getElementById("mobile-filter-toggle");
        if (x.innerHTML === "Filter") {
            x.innerHTML = "Hide Filter";
        } else {
            x.innerHTML = "Filter";
        }
    });
   
})(jQuery);


/*
* Replace all SVG images with inline SVG
*/
jQuery('img.svg').each(function(){
    var $img = jQuery(this);
    var imgID = $img.attr('id');
    var imgClass = $img.attr('class');
    var imgURL = $img.attr('src');

    jQuery.get(imgURL, function(data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find('svg');

        // Add replaced image's ID to the new SVG
        if(typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID);
        }
        // Add replaced image's classes to the new SVG
        if(typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass+' replaced-svg');
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a');

        // Replace image with new SVG
        $img.replaceWith($svg);

    }, 'xml');

});