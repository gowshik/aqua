/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'GHAICO\'">' + entity + '</span>' + html;
	}
	var icons = {
		'ghaico-ghaico-area': '&#xe900;',
		'ghaico-ghaico-arrow-down': '&#xe901;',
		'ghaico-ghaico-arrow-left': '&#xe902;',
		'ghaico-ghaico-arrow-right': '&#xe903;',
		'ghaico-ghaico-arrow-up': '&#xe904;',
		'ghaico-ghaico-social-facebook': '&#xe905;',
		'ghaico-ghaico-bathrooms': '&#xe906;',
		'ghaico-ghaico-bedrooms': '&#xe907;',
		'ghaico-ghaico-brief': '&#xe908;',
		'ghaico-ghaico-calculate': '&#xe909;',
		'ghaico-ghaico-carspaces': '&#xe90a;',
		'ghaico-ghaico-cevron-down': '&#xe90b;',
		'ghaico-ghaico-cevron-left': '&#xe90c;',
		'ghaico-ghaico-cevron-right': '&#xe90d;',
		'ghaico-ghaico-cevron-up': '&#xe90e;',
		'ghaico-ghaico-circle-arrow': '&#xe90f;',
		'ghaico-ghaico-circle-arrow-down': '&#xe910;',
		'ghaico-ghaico-circle-outline': '&#xe911;',
		'ghaico-ghaico-circle-solid': '&#xe912;',
		'ghaico-ghaico-climate': '&#xe913;',
		'ghaico-ghaico-close': '&#xe914;',
		'ghaico-ghaico-download': '&#xe915;',
		'ghaico-ghaico-fittings': '&#xe916;',
		'ghaico-ghaico-floorplan': '&#xe917;',
		'ghaico-ghaico-green-home': '&#xe918;',
		'ghaico-ghaico-home': '&#xe919;',
		'ghaico-ghaico-home-build': '&#xe91a;',
		'ghaico-ghaico-home-design': '&#xe91b;',
		'ghaico-ghaico-home-save': '&#xe91c;',
		'ghaico-ghaico-landscape': '&#xe91d;',
		'ghaico-ghaico-landscaping-durable': '&#xe91e;',
		'ghaico-ghaico-landscaping-unobtrusive': '&#xe91f;',
		'ghaico-ghaico-landscaping-versatile': '&#xe920;',
		'ghaico-ghaico-location': '&#xe921;',
		'ghaico-ghaico-menu': '&#xe922;',
		'ghaico-ghaico-minus': '&#xe923;',
		'ghaico-ghaico-play': '&#xe924;',
		'ghaico-ghaico-plus': '&#xe925;',
		'ghaico-ghaico-portal-design': '&#xe926;',
		'ghaico-ghaico-portal-faq': '&#xe927;',
		'ghaico-ghaico-portal-forms': '&#xe928;',
		'ghaico-ghaico-portal-marketing': '&#xe929;',
		'ghaico-ghaico-portal-menu': '&#xe92a;',
		'ghaico-ghaico-portal-partners': '&#xe92b;',
		'ghaico-ghaico-portal-reports': '&#xe92c;',
		'ghaico-ghaico-portal-sales': '&#xe92d;',
		'ghaico-ghaico-portal-shop': '&#xe92e;',
		'ghaico-ghaico-portal-toolbox': '&#xe92f;',
		'ghaico-ghaico-search': '&#xe930;',
		'ghaico-ghaico-search-location': '&#xe931;',
		'ghaico-ghaico-sign-out': '&#xe932;',
		'ghaico-ghaico-social-instagram': '&#xe933;',
		'ghaico-ghaico-social-pinterest': '&#xe934;',
		'ghaico-ghaico-social-twitter': '&#xe935;',
		'ghaico-ghaico-tick': '&#xe936;',
		'ghaico-ghaico-tools': '&#xe937;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/ghaico-[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
