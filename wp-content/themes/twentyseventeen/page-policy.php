<?php
/**
 * Template Name: Policy & Procedures
 *
*/
//get_header();
wp_head();
while ( have_posts() ) : the_post();
?>
<link href="https://file.myfontastic.com/5kX8TPLEh2hK7BdGxf37rg/icons.css" rel="stylesheet">
<div class="builder-portal-header mobile-head">
	<div class="container-fluid px-5">
		<div class="row position-relative">
			<div class="col-8 d-flex col-lg-3 col-xl-2 builder-portalh">
				<span class="d-flex d-lg-none my-auto text-white mr-2 ghaico-ghaico-portal-menu"></span>
				<h2 class="my-auto mx-0 text-white">Aqualand</h2>
			</div>

			<div class="col-6 d-none d-lg-flex col-lg-6 col-xl-8">
				<div class="portal-search w-100 my-auto">
					<?php get_search_form(); ?>
				</div>
			</div>

			<div class="col-3 d-none d-lg-flex col-lg-3 col-xl-2 builder-account">
				<div class="my-auto w-100 text-white text-right">
					<!--img src="<?php echo get_template_directory_uri() . '/assets/images/gha-icon-portal.png' ?>"-->
					<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>
				</div>
			</div>

			<div class="gha-builder-pdetails p-4 bg-white position-absolute">
				<p class="text-medium font-weight-bold">My details</p>
				<table class="mb-3">
					<tbody>
						<tr>
							<td class="py-1" style="width: 80px;">Phone</td>
							<td class="font-weight-bold">1300 724 661</td>
						</tr>

						<tr>
							<td class="py-1" style="width: 80px;">Email</td>
							<td class="font-weight-bold">support@greenhomesaustralia.com.au</td>
						</tr>

						<tr>
							<td class="py-1" style="width: 80px;">Address</td>
							<td class="font-weight-bold">125 Byng Street, Orange, NSW 2800</td>
						</tr>
					</tbody>
				</table>

				<p class="text-medium font-weight-bold">
					<a href="<?php echo wp_logout_url(home_url( '/builder-area' )); ?>">Logout <span class="ghaico-ghaico-sign-out"></span></a>
				</p>
			</div>

		</div>
	</div>
</div>

<div class="builder-potal-body">
	<div class="container-fluid">
		<div class="row">





			<div class="d-lg-block col-lg-2 portal-menu pt-4 px-0 bg-white menu-small">

				<div class="d-block  d-lg-none my-3 px-3 w-100">
					<div class="sm-bdetals" style="border-bottom: 1px solid #DDDDDD; padding-bottom: 1rem;">
						<div>
							<img class="mr-2" src="<?php echo get_template_directory_uri() . '/assets/images/gha-portal-icon-alt.png' ?>"> GHA Orange
							<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>
							<span class="icon-chevron-down"></span>

						</div>

						<div class="gha-builder-pdetails p-4">
							<p class="text-medium font-weight-bold">My details</p>
							<table class="mb-3">
								<tbody>
									<tr>
										<td class="py-1" style="width: 80px;">Phone</td>
										<td class="font-weight-bold">1300 724 661</td>
									</tr>

									<tr>
										<td class="py-1" style="width: 80px;">Email</td>
										<td class="font-weight-bold">support@greenhomesaustralia.com.au</td>
									</tr>

									<tr>
										<td class="py-1" style="width: 80px;">Address</td>
										<td class="font-weight-bold">125 Byng Street, Orange, NSW 2800</td>
									</tr>
								</tbody>
							</table>

							<p class="text-medium font-weight-bold">
								<a href="#">Logout <span class="ghaico-ghaico-sign-out"></span></a>
							</p>
						</div>

					</div>
				</div>
				<div class="logo-holder" style="padding-bottom:30px;">
					<center><img src="<?php echo get_template_directory_uri() . '/assets/images/logo-white.png'; ?>"></center>
				</div>

				<?php
					wp_nav_menu( array(
					    'theme_location' => 'builder_area',
					) );
				?>
			</div>

			<div class=" col-lg-10 " style="margin-bottom: 80px;padding-right:5px;padding-left:2px">
				<div class="builder-portal-header desktop-head">
				<div class="row position-relative">


					<div class="col-10 d-none d-lg-flex col-lg-10 ">
						<div class="portal-search w-100 my-auto">
							<?php get_search_form(); ?>
						</div>
					</div>

					<div class="col-2 d-none d-lg-flex col-lg-2  builder-account">
						<div class="my-auto w-100 text-white text-right">

							<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>

						</div>
					</div>

					<div class="gha-builder-pdetails p-4 bg-white position-absolute">
						<p class="text-medium font-weight-bold" style="color:#274279;">My details</p>
						<table class="mb-3">
							<tbody>
								<tr>
									<td class="py-1" style="width: 80px;">Phone</td>
									<td class="font-weight-bold">02 9228 1888</td>
								</tr>

								<tr>
									<td class="py-1" style="width: 80px;">Email</td>
									<td class="font-weight-bold">info@aqualand.com.au</td>
								</tr>

								<tr>
									<td class="py-1" style="width: 80px;">Address</td>
									<td class="font-weight-bold">Tower One, Level 47,<br> 100 Barangaroo Avenue, <br>Barangaroo NSW 2000</td>
								</tr>
							</tbody>
						</table>

						<p class="text-medium font-weight-bold">
							<a style="color:#274279;" href="<?php echo wp_logout_url(home_url( '/aqualand' )); ?>">Logout <span class="ghaico-ghaico-sign-out"></span></a>
						</p>
					</div>
				</div>
				</div>
				<div class="row" style="padding-left: 12px;">
				<div class="d-lg-block col-lg-2 portal-menu pt-4 px-0 bg-white menu-small" style="background-color:#0072BB !important">
					<div class="menu-builder-area-container">
						<ul id="menu-builder-area-1" class="menu">
							<li class=" d-flex flex-row menu-item menu-item-type-post_type menu-item-object-page "><a href="">HUMAN RESOURCES</a></li>
							<li class="d-flex flex-row menu-item menu-item-type-post_type menu-item-object-page"><a href="">RESPONSIBILITIES</a></li>
							<li class=" d-flex flex-row menu-item menu-item-type-post_type menu-item-object-page "><a href="">HEALTH &amp; SAFETY</a></li>
							<li class=" d-flex flex-row menu-item menu-item-type-post_type menu-item-object-page "><a href="">POLICIES</a></li>
							<li class=" d-flex flex-row menu-item menu-item-type-post_type menu-item-object-page "><a href="">LEAVE</a></li>

						</ul>
					</div>
				</div>
				<div class="col-10 d-none d-lg-flex col-lg-10  builder-account">


				<div class="builder-portal-resources" style="padding-top:80px;padding-right:100px;padding-left:30px; width:100%">
					<div class="search-container" style="padding-left: 40px;">
<form action="" style="max-width:100%">
	<span class="search-place-icon ghaico-ghaico-search" style="padding:4px 10px;font-size:14px;"></span>
<input type="text" placeholder="Find document" name="search" style="width:100%;padding-left: 30px;border-radius: 10px;border: none;font-size:14px">
<button id ="edit-custom-search-blocks"type="submit"><i class="fa fa-search" style="color:#092140"></i></button><hr>
</form>
</div>
<div class="files-div">
	<ul class="file_submit">
		<li style="font-size: 14px;">Download Selected</li>
		<li style="font-size: 14px;">View in Browser</li>
	</ul>
</div>
					<ul>
						<table>

						<?php

	// check if the repeater field has rows of data
	if( have_rows('files') ):?>
	<tr style="border-bottom: 2px solid #F2F2F2;">
		<th style="padding-left:10px;"><input class="chkAll" type="checkbox" name="files[]" value="<?php echo $file_d['url']; ?>"/><br /></th>
		<th>NAME</th>
		<th>LAST MODIFIED</th>
		<th>TYPE</th>
		<th>SIZE</th>
	</tr>

	 <?php	// loop through the rows of data
	    while ( have_rows('files') ) : the_row();
					?>

					<tr>
						<?php
	        // display a sub field value

	        $file_d=get_sub_field('downloads');
						$attachment_id=$file_d['id'];
					//	var_dump($attachment_id);
					//var_dump($file_d);
					$filesize = filesize(get_attached_file( $attachment_id ));
					$path_info = pathinfo( get_attached_file( $attachment_id ) );
					$date = get_the_date('Y/m/d  g:i', $attachment_id);

					$filesize = size_format($filesize, 2);
				//	var_dump($filesize);
					if( $file_d): ?>
					<td>
<input class="chk" type="checkbox" name="files[]" value="<?php echo $file_d['url']; ?>"/><br />
</td>
	<!--a download="<?php echo $file_d['title']; ?>" href="<?php echo $file_d['url']; ?>"><li><?php echo $file_d['title']; ?><li></a-->
	<td>
		<?php echo $file_d['title']; ?>
	</td>
<td>
	<?php echo $date; ?>
</td>
<td>
	<?php echo $path_info['extension']; ?>
</td>
<td>
	<?php echo $filesize; ?>
</td>
<?php endif;
?>
</tr>
<?php
	    endwhile;


	endif;

	?>
</table>
					</ul>



					<?php if( get_field('gha_builder_area_additional_information') ) { ?>
						<div class="additional-info p-4 bg-white">
							<?php the_field('gha_builder_area_additional_information'); ?>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
			</div>


		</div>
	</div>
</div>
<style>
.builder-portal-header .search-form input.form-control {
    height: 60px;
  	  text-indent: 25px;
    font-size: 12px;
    color: #999999;
    border-radius: 4px;
    border:none;
    width: 100%;
}
.builder-portal-header{
	background:#fff;
	    height: 60px;
}
.builder-potal-body{
	margin-top:30px;
}

body{
	margin-top:-30px;
}
.menu-small{
	background-color:#274279 !important;
	min-height: 100%;
}
.portal-menu .menu li a {
    display: flex;
    width: 100%;
    padding: 20px 15px 20px 15px;
    margin: auto 0;
    font-family: 'Lato', sans-serif;
    font-size: 16px;
    color: #fff;
}
.portal-menu .menu li.current-menu-item {
    /*background: #213766;*/
		  background:#1B2D52;
}
.logo-holder{
	padding:10px;
	overflow:hidden;


}
.logo-holder img{
	margin:auto;
	align-items: center;
}
.portal-menu .menu li:before {
    margin: auto;
    font-size: 24px;
    margin-left: 30px;
    color: #fff;
}
.ghaico-ghaico-cevron-down:before {
    content: "\e90b";
		color:#222;
}
span.search-place-icon {
    position: absolute;
    padding: 0 10px;
    font-size: 20px;
    color: #999;
}
@media only screen
  and (min-device-width: 769px)

 {


  .mobile-head{
    display:none;
  }
}
.builder-portal-resources table {
    border-collapse: collapse;
    width: 100%;
}

.builder-portal-resources td {
    text-align: left;
    padding: 8px;
    background:#fff;
		font-size:14px;

}
.builder-portal-resources th{
	text-align: left;
	padding: 8px;
	background:#fff;
	font-size:16px;
	color:#0072BB;
}

tr:nth-child(even){/*background-color: #f2f2f2*/}
.builder-portal-resources tr{

  border-spacing: 5px;
  padding-top: .5em;
  padding-bottom: .5em;
}
.builder-portal-resources table tr {
  border-bottom: 1px solid #F2F2F2;
	height:80px;
}

.builder-portal-resources table tr:last-child {
  border: 0;
}
#edit-custom-search-blocks {
      position: absolute;
      top: -999px;
      left: -999px;
      width: 0px;
      height: 0px;
}
.file_submit li{
	display:inline-block;
	padding-right:50px;
}
.files-div{
	padding-top:10px;
	padding-bottom:10px;
}
.portal-menu .menu li a:after {
display:none;
}




</style>

<!--script src="http://code.jquery.com/jquery-1.11.0.min.js"></script-->
<script type="text/javascript">
jQuery(".chkAll").click(function () {
		jQuery('input:checkbox').not(this).prop('checked', this.checked);
});

/*
$('#submit').prop("disabled", true);
$("#checkAll").change(function () {
      $("input:checkbox").prop('checked', $(this).prop("checked"));
	  $('#submit').prop("disabled", false);
	  if ($('.chk').filter(':checked').length < 1){
			$('#submit').attr('disabled',true);}
});

$('input:checkbox').click(function() {
        if ($(this).is(':checked')) {
			$('#submit').prop("disabled", false);
        } else {
		if ($('.chk').filter(':checked').length < 1){
			$('#submit').attr('disabled',true);}
		}
});*/
/*var menuItems = document.querySelectorAll('li.menu-item-has-children');
Array.prototype.forEach.call(menuItems, function(el, i){
	el.addEventListener("mouseover", function(event){
		this.className = "menu-item-has-children open";
		clearTimeout(timer);
	});
	el.addEventListener("mouseout", function(event){
		timer = setTimeout(function(event){
			document.querySelector(".menu-item-has-children.open").className = "menu-item-has-children";
		}, 1000);
	});
});*/
</script>
<?php
endwhile;
wp_footer();
