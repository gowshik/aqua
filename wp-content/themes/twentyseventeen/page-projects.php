<?php
/**
 * Template Name: Projects
 *
*/
//get_header();
wp_head();
while ( have_posts() ) : the_post();
?>
<link href="https://file.myfontastic.com/5kX8TPLEh2hK7BdGxf37rg/icons.css" rel="stylesheet">
<div class="builder-portal-header mobile-head">
	<div class="container-fluid px-5">
		<div class="row position-relative">
			<div class="col-8 d-flex col-lg-3 col-xl-2 builder-portalh">
				<span class="d-flex d-lg-none my-auto text-white mr-2 ghaico-ghaico-portal-menu"></span>
				<h2 class="my-auto mx-0 text-white">Aqualand</h2>
			</div>

			<div class="col-6 d-none d-lg-flex col-lg-6 col-xl-8">
				<div class="portal-search w-100 my-auto">
					<?php get_search_form(); ?>
				</div>
			</div>

			<div class="col-3 d-none d-lg-flex col-lg-3 col-xl-2 builder-account">
				<div class="my-auto w-100 text-white text-right">
					<!--img src="<?php echo get_template_directory_uri() . '/assets/images/gha-icon-portal.png' ?>"-->
					<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>
				</div>
			</div>

			<div class="gha-builder-pdetails p-4 bg-white position-absolute">
				<!--p class="text-medium font-weight-bold">My details</p>
				<table class="mb-3">
					<tbody>
						<tr>
							<td class="py-1" style="width: 80px;">Phone</td>
							<td class="font-weight-bold">1300 724 661</td>
						</tr>

						<tr>
							<td class="py-1" style="width: 80px;">Email</td>
							<td class="font-weight-bold">support@greenhomesaustralia.com.au</td>
						</tr>

						<tr>
							<td class="py-1" style="width: 80px;">Address</td>
							<td class="font-weight-bold">125 Byng Street, Orange, NSW 2800</td>
						</tr>
					</tbody>
				</table-->

				<p class="text-medium font-weight-bold">
					<a href="<?php echo wp_logout_url(home_url( '/aqualand' )); ?>">Logout <span class="ghaico-ghaico-sign-out"></span></a>
				</p>
			</div>

		</div>
	</div>
</div>

<div class="builder-potal-body">
	<div class="container-fluid">
		<div class="row">





			<div class="d-lg-block col-lg-2 portal-menu pt-4 px-0 bg-white menu-small">

				<!--div class="d-block  d-lg-none my-3 px-3 w-100">
					<div class="sm-bdetals" style="border-bottom: 1px solid #DDDDDD; padding-bottom: 1rem;">
						<div>
							<img class="mr-2" src="<?php echo get_template_directory_uri() . '/assets/images/gha-portal-icon-alt.png' ?>"> GHA Orange
							<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>
							<span class="icon-chevron-down"></span>

						</div>

						<div class="gha-builder-pdetails p-4">
							<p class="text-medium font-weight-bold">My details</p>
							<table class="mb-3">
								<tbody>
									<tr>
										<td class="py-1" style="width: 80px;">Phone</td>
										<td class="font-weight-bold">1300 724 661</td>
									</tr>

									<tr>
										<td class="py-1" style="width: 80px;">Email</td>
										<td class="font-weight-bold">support@greenhomesaustralia.com.au</td>
									</tr>

									<tr>
										<td class="py-1" style="width: 80px;">Address</td>
										<td class="font-weight-bold">125 Byng Street, Orange, NSW 2800</td>
									</tr>
								</tbody>
							</table>

							<p class="text-medium font-weight-bold">
								<a href="#">Logout <span class="ghaico-ghaico-sign-out"></span></a>
							</p>
						</div>

					</div>
				</div-->
				<div class="logo-holder" style="padding-bottom:30px;">
					<center><img src="<?php echo get_template_directory_uri() . '/assets/images/logo-white.png'; ?>"></center>
				</div>

				<?php
					wp_nav_menu( array(
					    'theme_location' => 'builder_area',
					) );
				?>
			</div>

			<div class=" col-lg-10 " style="margin-bottom: 80px;padding-right:5px;padding-left:2px">
				<div class="builder-portal-header desktop-head">
				<div class="row position-relative">


					<div class="col-10 d-none d-lg-flex col-lg-10 ">
						<div class="portal-search w-100 my-auto">
							<?php get_search_form(); ?>
						</div>
					</div>

					<div class="col-2 d-none d-lg-flex col-lg-2  builder-account">
						<div class="my-auto w-100 text-white text-right">

							<span class="ghaico-ghaico-cevron-down ml-2 align-middle"></span>

						</div>
					</div>

					<div class="gha-builder-pdetails p-4 bg-white position-absolute">
						<p class="text-medium font-weight-bold" style="color:#274279;">My details</p>
						<table class="mb-3">
							<tbody>
								<tr>
									<td class="py-1" style="width: 80px;">Phone</td>
									<td class="font-weight-bold">02 9228 1888</td>
								</tr>

								<tr>
									<td class="py-1" style="width: 80px;">Email</td>
									<td class="font-weight-bold">info@aqualand.com.au</td>
								</tr>

								<tr>
									<td class="py-1" style="width: 80px;">Address</td>
									<td class="font-weight-bold">Tower One, Level 47,<br> 100 Barangaroo Avenue, <br>Barangaroo NSW 2000</td>
								</tr>
							</tbody>
						</table>

						<p class="text-medium font-weight-bold">
							<a style="color:#274279;" href="<?php echo wp_logout_url(home_url( '/aqualand' )); ?>">Logout <span class="ghaico-ghaico-sign-out"></span></a>
						</p>
					</div>
				</div>
				</div>

				<div class="builder-portal-resources">
					<ul>
						<?php
							if( have_rows('gha_builder_area_resources') ) :
								while ( have_rows('gha_builder_area_resources') ) : the_row();
						?>
									<li class="mb-4 bg-white"><a class="d-block p-4 ghaico-ghaico-download" href="<?php the_sub_field('gha_builder_area_resource_link'); ?>" target="_blank"><?php the_sub_field('gha_builder_area_resource_title'); ?></a></li>
						<?php
								endwhile;
							endif;
						?>
					</ul>
          <section id="posts" style="padding-top:40px;padding-bottom:100px;">
            <div class="container news-list" style="">
              <div>
                <h2 class="decorated"><span>SHOW ALL</span></h2>
              </div>
              <div class="row category-news">
                <div class="col"><input type="radio" name="r" id="r1"><label for="r1">Complete</label></div>
                <div class="col"><input type="radio" name="r" id="r2"><label for="r2">Current</label></div>
                <div class="col"><input type="radio" name="r" id="r3"><label for="r3">Future</label></div>
                <div class="col"><input type="radio" name="r" id="r4"><label for="r4">Investment</label></div>
              </div>
              <div class="row" style="padding-top:40px;">
                <div class="row no-gutters">
                  <div class="col-md-12">
                    <?php $args2 = array( 'order' => 'DESC','posts_per_page' => '8','post_type'=>'project');?>
                    <?php $q =new WP_query ( $args2 );
                    if ($q->have_posts() ) : ?>
                      <div class="row no-gutters" id="news-box" style="border-top:none;">
                        <?php while ($q->have_posts()) : $q->the_post(); ?>
                          <div class="col-md-3 link-box" style="">
                            <?php $tags = wp_get_post_tags($post->ID);
                              //var_dump($tags);
                              $t=$tags[0]->name;
                              //var_dump($t); ?>
                              <?php //if ( has_post_thumbnail()) {
                              //$url = wp_get_attachment_url( get_post_thumbnail_id() );
                               ?>
                            <div class="news_holder"style=""><?php the_post_thumbnail(array(900,500));?></div>
                              <?php //} ?>

                                  <div class="short_desc">
                                      <p class="news_title"><?php echo wp_trim_words( get_the_title(), 4 ); ?></p>
                                      </div>



                      </div>

    <?php endwhile; ?>
</div>
</div>
</div>
</div>
</div>
  </section>
  <?php endif;  ?>

					<?php if( get_field('gha_builder_area_additional_information') ) { ?>
						<div class="additional-info p-4 bg-white">
							<?php the_field('gha_builder_area_additional_information'); ?>
						</div>
					<?php } ?>
				</div>
			</div>


		</div>
	</div>
</div>
<style>
.builder-portal-header .search-form input.form-control {
    height: 60px;
  	  text-indent: 25px;
    font-size: 12px;
    color: #999999;
    border-radius: 4px;
    border:none;
    width: 100%;
}
.builder-portal-header{
	background:#fff;
	    height: 60px;
}
.builder-potal-body{
	margin-top:30px;
}

body{
	margin-top:-30px;
}
.menu-small{
	background-color:#274279 !important;
	min-height: 100%;
}
.portal-menu .menu li a {
    display: flex;
    width: 100%;
    padding: 20px 15px 20px 15px;
    margin: auto 0;
    font-family: 'Lato', sans-serif;
    font-size: 16px;
    color: #fff;
}
.portal-menu .menu li.current-menu-item {
    /*background: #213766;*/
		  background:#1B2D52;
}
.logo-holder{
	padding:10px;
	overflow:hidden;


}
.logo-holder img{
	margin:auto;
	align-items: center;
}
.portal-menu .menu li:before {
    margin: auto;
    font-size: 24px;
    margin-left: 30px;
    color: #fff;
}
.ghaico-ghaico-cevron-down:before {
    content: "\e90b";
		color:#222;
}
.news_holder{
  overflow:hidden!important;
  height: 200px;


}
.news_holder img{
  height:100% !important;
  width:100% !important;
  object-fit:cover;
}
.news-list{
  max-width:1350px;
}
.container{
          margin:auto;
}
.link-box{
   padding:10px !important;

}
.short_desc{
    padding-bottom:20px;



}
.category-news{

    max-width:520px;
    margin-right:0;
    margin-left:auto;
    margin-top:-30px;

    text-align: left;

}
.category-news button {
    background:none!important;
     border:none;
     padding:0!important;
     color:#274279;
    font-size:24px;
    font-weight:500;


     cursor:pointer;
}
.decorated{

    color:#274279;
     overflow: hidden;

 }
.decorated > span{
    position: relative;
    display: inline-block;
}
.news_title{
  font-size:20px;
  color:#274279;
  font-weight:400;
  line-height: 1 !important;
  margin-bottom:2px;
  padding-top:20px;
}
.news_info p{
  padding-top:20px;
  color:#4D5474;
  font-weight:300 !important;
  font-size:14px !important;
  line-height:2 !important;
  padding-bottom:20px;
}
.news_date{
  color:#70768F;
  font-size:12px;
  font-weight:400;
  text-transform:uppercase;

}
.read-more a{
  border:2px solid #0072BB;
  display:block;
  color:#0072BB;
  width:90px;
  padding:8px;

}
.read-more p{
  margin-top:0;
  margin-bottom:0;
  font-size:14px;
}
span.search-place-icon {
    position: absolute;
    padding: 0 10px;
    font-size: 20px;
    color: #999;
}
@media only screen
  and (min-device-width: 769px)

 {


    .mobile-head{
    display:none;
  }
}
input[type=radio]{
  /* hide original inputs */
  visibility: hidden;
  position: absolute;
}
input[type=radio] + label{
  cursor:pointer;
}
input[type=radio] + label:before
{
  height:16px;
  margin-right: 4px;
  content: " ";
  display:inline-block;
  vertical-align: baseline;
  transition: 0.3s;
  border:1px solid #ccc;
  border-radius:10px;
  box-shadow: inset 0 -3px 6px #e4e4e4;
  transition: 0.3s;
}

/* CUSTOM RADIO AND CHECKBOX STYLES */
/* DEFAULT */
input[type=radio] + label:before{
  border-radius:50%;
  width:16px;
}

/* CHECKED */
input[type=radio]:checked + label:before{
  box-shadow: inset 0 -1px 3px #e4e4e4, inset 0 0 1px #222, inset 0 0 0 3px #274279;
}
.portal-menu .menu li a:after {
display:none;
}


</style>
<?php
endwhile;
wp_footer();
