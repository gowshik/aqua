<?php
/**
 * Template for displaying search forms in Twenty Seventeen
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form role="search" method="get" class="search-form form-inline" action="">
	<span class="search-place-icon ghaico-ghaico-search"></span>
	<input class="form-control" type="search" name="keyword" placeholder="Search" aria-label="Search">
</form>
